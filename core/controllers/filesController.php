<?php
include_once 'core/models/class.File.php';
include_once 'core/models/class.Category.php';

if(isset($_SESSION['id_user'])){

    $isset_id = isset($_GET['id']) and is_numeric($_GET['id']) and $_GET['id'] >= 1;

    $file = new File();

    switch (isset($_GET['mode']) ?  $_GET['mode']: null) {

        case 'agregar':
            if($_SESSION['level'] < ADMIN_LEVEL){
                header('location: ?view=files');
                die();
            }
            $categories = Category::get_categories();
            if($_POST and $_FILES){
                if(!isset($_POST['name']) || !isset($_FILES['file']) || !isset($_POST['category']) || !isset($_POST['category_name'])){
                    header('location: ?view=files');
                }

                $destiny = MEDIA_FOLDER . "files/" . slugify($_POST['category_name']) . "/";
                if (!file_exists($destiny)) {
                    mkdir($destiny, 0777, true);
                }
                opendir($destiny);
                $file_name = $destiny . $_FILES['file']['name'];
                try{
                    copy($_FILES['file']['tmp_name'], $file_name);
                    $file->file = $file_name;
                }catch (Exception $e){

                }

                $file->name = $_POST['name'];
                $file->category = $_POST['category'];
                $file->create();
                header('location: ?view=files&mode=agregar&success=true');
            }
            else {
                include(HTML_DIR . 'dashboard/files/add.file.php');
            }
            break;

        case 'editar':
            if($_SESSION['level'] < ADMIN_LEVEL){
                header('location: ?view=files');
                die();
            }
            if($isset_id){
                if($_POST){
                    if(!isset($_POST['name']) || !isset($_POST['category']) || !isset($_POST['category_name']) ){
                        header('location: ?view=files');
                    }
                    $file->id = $_GET['id'];
                    $file->name = $_POST['name'];
                    $file->category = $_POST['category'];
                    $file->category_name = $_POST['category_name'];

                    if($_FILES['file']['size'] != 0 and $_FILES['file']['error'] == 0){
                        $file->update(true);
                    }else{
                        $file->update(false);
                    }
                    header('location: ?view=files&mode=editar&success=true&id=' . $_GET['id']);
                }
                else {
                    $categories = Category::get_categories();
                    $file->id = $_GET['id'];
                    $file_data = $file->retrieve();
                    include(HTML_DIR . 'dashboard/files/edit.file.php');
                }
            } else {
                header('location: ?view=files');
            }
            break;

        case 'borrar':
            if($_SESSION['level'] < ADMIN_LEVEL){
                header('location: ?view=files');
                die();
            }
            if($isset_id){
                $file->id = $_GET['id'];
                $file->delete();
                header('location: ?view=files&mode=delete&success=true');
            }
            else {
                header('location: ?view=users');
            }
            break;

        default:
            include(HTML_DIR . 'dashboard/files.php');
            break;
    }
}else {
    header('location: ?view=index');
}

