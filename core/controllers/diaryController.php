<?php
include_once 'core/models/class.Diary.php';

if(isset($_SESSION['id_user'])){

    $isset_id = isset($_GET['id']) and is_numeric($_GET['id']) and $_GET['id'] >= 1;

    $ajax = isset($_GET['ajax']) and $_GET['ajax'] == true;

    $diary = new Diary();

    switch (isset($_GET['mode']) ?  $_GET['mode']: null) {

        case 'agregar':

            if($_POST){
                if(!isset($_POST['contact_date']) and !isset($_POST['company'])){
                    include(HTML_DIR . 'dashboard/contact-list.php');
                }

                $_POST['contact_date'] = change_date_format_to_db($_POST['contact_date']);
                $_POST['next_visit'] = isset($_POST['next_visit']) ?  change_datetime_format($_POST['next_visit']) : null;
                $_POST['visit'] = isset($_POST['visit']) ?  change_datetime_format($_POST['visit']) : null;

                foreach ($_POST as $key => $value){

                    $diary->$key =  $value;
                }
                $diary->user = $_SESSION['id_user'];
                $response = $diary->create();
                if($response == false){
                    header('location: ?view=diary&success=true');
                }else{
                    header('location: ?view=diary&success=false&error='. slugify($response));
                }
            }
            else {
                if($_SESSION['level'] >= ADMIN_LEVEL){
                    include(HTML_DIR . 'dashboard/diary/add.diary.php');
                }else{
                    include(HTML_DIR . 'agents/diary/agent.add.diary.php');
                }
            }
            break;

        case 'editar':

            if($isset_id){
                if($_POST){
                    if(!isset($_POST['contact_date']) and !isset($_POST['company'])){
                        include(HTML_DIR . 'dashboard/diary.php');
                    }

                    foreach ($_POST as $key => $value){

                        $diary->$key =  $value;
                    }
                    $diary->user = $_SESSION['id_user'];
                    $diary->id = $_GET['id'];
                    $diary->update();

                    header('location: ?view=diary&mode=editar&success=true&id=' . $_GET['id']);
                }
                else {
                    $diary->id = $_GET['id'];
                    $diary_data = $diary->retrieve();
                    if ($ajax){
                        echo json_encode($diary_data);
                        die();
                    }else{

                        if($_SESSION['level'] >= ADMIN_LEVEL){
                            include(HTML_DIR . 'dashboard/diary/edit.diary.php');
                        }else{
                            include(HTML_DIR . 'agents/diary/agent.edit.diary.php');
                        }
                    }
                }
            } else {
                header('location: ?view=diary');
            }


            break;

        case 'borrar':
            if($isset_id){
                $diary->id = $_GET['id'];
                $diary->delete();

                if($_SESSION['level'] >= ADMIN_LEVEL){
                    header('location: ?view=diary&mode=delete&success=true');
                }else{
                    header('location: ?view=agents&mode=borrar&object=visita');
                }

            }
            else {
                header('location: ?view=diary');
            }
            break;
            break;

        default:
            if($_SESSION['level'] >= ADMIN_LEVEL){
                include(HTML_DIR . 'dashboard/diary.php');
            }else{
                include(HTML_DIR . 'agents/agent.diary.php');
            }

            break;
    }
}else {
    header('location: ?view=index');
}

