<?php
include_once 'core/models/class.Sale.php';
include_once 'core/models/class.User.php';

if(isset($_SESSION['id_user'])){

    $isset_id = isset($_GET['id']) and is_numeric($_GET['id']) and $_GET['id'] >= 1;

    if(isset($_GET['user'])){
        $user = new User($_GET['user']);
        $user = $user->retrieve();
    }

    $sale = new Sale();

    if($isset_id){
        $sale->id = $_GET['id'];
        $user_sale = $sale->retrieve();

        $_GET['mode'] = ($user_sale['user'] == $_SESSION['id_user']) ? $_GET['mode'] : null;
    }

    switch (isset($_GET['mode']) ?  $_GET['mode']: null) {

        case 'agregar':

            if($_POST){

                if(!isset($_POST['clients']) || !isset($_POST['company']) || !isset($_POST['policy_number']) ||
                    !isset($_POST['business_type']) || !isset($_POST['transaction_number']) || !isset($_POST['date']) ){
                    header('location: ?view=sales');
                }
                $_POST['date'] = change_date_format_to_db($_POST['date']);
                foreach ($_POST as $key => $value){

                    $sale->$key =  $value;
                }
                $sale->user = $_SESSION['id_user'];
                $response = $sale->create();
                if ($_SESSION['level'] >= ADMIN_LEVEL){
                    if($response == false){
                        header('location: ?view=sales&success=true');
                    }else{
                        header('location: ?view=sales&success=false&error='. slugify($response));
                    }
                }else{
                    if($response == false){
                        echo json_encode(array(
                            'response' => 'success',
                            'msg' => _('La venta se ha agregado exitosamente!'),
                            'id' => 'sale_created_' . $sale->id
                        ));
                    }else{
                        echo json_encode(array(
                            'response' => 'error',
                            'msg' => $response
                        ));
                    }
                    die();
                }


            }
            else {
                include(HTML_DIR . 'dashboard/sales.php');
            }
            break;

        case 'editar':
            if($isset_id){
                if($_POST){
                    $user->id = $_GET['id'];
                    $user->first_name = $_POST['first_name'];
                    $user->last_name = $_POST['last_name'];
                    $user->email = $_POST['email'];
                    $user->profile = $_POST['profile'];
                    $user->password = $_POST['password'];
                    $user->username = $_POST['username'];
                    $user->update();

                    header('location: ?view=users&mode=editar&success=true&id=' . $_GET['id']);
                }
                else {
                    $user->id = $_GET['id'];
                    $user_data = $user->retrieve();
                    include(HTML_DIR . 'dashboard/users/edit.user.php');
                }
            } else {
                header('location: ?view=users');
            }
            break;

        case 'borrar':
            if($isset_id){
                $sale->id = $_GET['id'];
                $sale->delete();
                if($_SESSION['level'] >= ADMIN_LEVEL){
                    header('location: ?view=sales&mode=borrar&success=true');
                }else{
                    header('location: ?view=agents&mode=borrar&object=venta');
                }

            }
            else {
                header('location: ?view=sales');
            }
            break;

        default:
            include(HTML_DIR . 'dashboard/sales.php');
            break;
    }

}else {
    header('location: ?view=index');
}

