<?php
include_once 'core/models/class.Contact.php';
include_once 'core/models/class.Category.php';

if(isset($_SESSION['id_user'])){

    $isset_id = isset($_GET['id']) and is_numeric($_GET['id']) and $_GET['id'] >= 1;

    $ajax = isset($_GET['ajax']) and $_GET['ajax'] == true;

    $contact = new Contact();

    switch (isset($_GET['mode']) ?  $_GET['mode']: null) {

        case 'agregar':
            if($_POST){
                if(!isset($_POST['phone']) and !isset($_POST['full_name'])){
                    include(HTML_DIR . 'dashboard/contact-list.php');
                }

                foreach ($_POST as $key => $value){

                    $contact->$key =  $value;
                }
                if($contact->birth){
                    $contact->birth = change_date_format_to_db($contact->birth);
                }


                $contact->user = $_SESSION['id_user'];
                $response = $contact->create();

                if ($_SESSION['level'] >= ADMIN_LEVEL){
                    if($response == false){
                        header('location: ?view=contacts&success=true');
                    }else{
                        header('location: ?view=contacts&success=false&error='. slugify($response));
                    }
                }else{
                    if($response == false){
                        echo json_encode(array(
                            'response' => 'success',
                            'msg' => _('El contacto se ha agregado exitosamente!'),
                            'id' => 'contact_created_' . $contact->id
                        ));
                    }else{
                        echo json_encode(array(
                            'response' => 'error',
                            'msg' => $response
                        ));
                    }
                    die();
                }
            }
            else {
                include(HTML_DIR . 'dashboard/contact-list.php');
            }
            break;

        case 'editar':

            if($isset_id){
                if($_POST){
                    if(!isset($_POST['phone']) and !isset($_POST['full_name'])){
                        include(HTML_DIR . 'dashboard/contact-list.php');
                    }

                    foreach ($_POST as $key => $value){

                        $contact->$key =  $value;
                    }
                    $contact->user = $_SESSION['id_user'];
                    $contact->id = $_GET['id'];

                    if($contact->birth){
                        $contact->birth = change_date_format_to_db($contact->birth);
                    }


                    $contact->update();

                    header('location: ?view=contacts&mode=editar&success=true&id=' . $_GET['id']);
                }
                else {
                    $contact->id = $_GET['id'];
                    $contact_data = $contact->retrieve();
                    if ($ajax){
                        echo json_encode($contact_data);
                        die();
                    }else{
                        if($_SESSION['level'] >= ADMIN_LEVEL){
                            include(HTML_DIR . 'dashboard/contacts/edit.contact.php');
                        }else{
                            include(HTML_DIR . 'agents/contacts/agent.edit.contact.php');
                        }
                    }
                }
            } else {
                header('location: ?view=contacts');
            }


            break;

        case 'borrar':
            if($isset_id){
                $contact->id = $_GET['id'];
                $contact->delete();
                if($_SESSION['level'] >= ADMIN_LEVEL){
                    header('location: ?view=contacts&mode=delete&success=true');
                }else{
                    header('location: ?view=agents&mode=borrar&object=contacto');
                }

            }
            else {
                header('location: ?view=contacts');
            }
            break;
            break;

        default:
            include(HTML_DIR . 'dashboard/contact-list.php');
            break;
    }
}else {
    header('location: ?view=index');
}
