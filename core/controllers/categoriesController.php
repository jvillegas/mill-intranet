<?php
include_once 'core/models/class.Category.php';

if(isset($_SESSION['id_user']) and $_SESSION['level'] >= ADMIN_LEVEL){

    $isset_id = isset($_GET['id']) and is_numeric($_GET['id']) and $_GET['id'] >= 1;

    $category = new Category();

    switch (isset($_GET['mode']) ?  $_GET['mode']: null) {

        case 'agregar':

            if($_POST){

                if(!isset($_POST['name'])){
                    header('location: ?view=categories');
                }
                $category->name = $_POST['name'];
                $category->create();
                header('location: ?view=categories&mode=agregar&success=true');
            }
            else {
                include(HTML_DIR . 'dashboard/categories/add.category.php');
            }
            break;

        case 'editar':
            if($isset_id){
                if($_POST){
                    $category->id = $_GET['id'];
                    $category->name = $_POST['name'];
                    $category->update();

                    header('location: ?view=categories&mode=editar&success=true&id=' . $_GET['id']);
                }
                else {
                    $category->id = $_GET['id'];
                    $category_data = $category->retrieve();
                    include(HTML_DIR . 'dashboard/categories/edit.category.php');
                }
            } else {
                header('location: ?view=categories');
            }
            break;

        case 'borrar':
            if($isset_id){
                $category->id = $_GET['id'];
                $category->delete();
                header('location: ?view=categories&mode=delete&success=true');
            }
            else {
                header('location: ?view=categories');
            }
            break;

        default:
            include(HTML_DIR . 'dashboard/categories.php');
            break;
    }
}else {
    header('location: ?view=index');
}

