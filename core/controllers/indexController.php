<?php
if(!isset($_SESSION) or !isset($_SESSION['level'])){
    include ('html/index/index.php');
}else{
    if ($_SESSION['level'] >= ADMIN_LEVEL){
        include ('html/index/index.php');
    }else{
        include_once 'core/models/class.Category.php';
        $files = Category::get_all_categories_and_files();
        include ('html/agents/agents.php');
    }
}


?>
