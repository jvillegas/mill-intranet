<?php
include_once 'core/models/class.User.php';
include_once 'core/models/class.Category.php';

if(isset($_SESSION['id_user'])){

    $isset_id = isset($_GET['id']) and is_numeric($_GET['id']) and $_GET['id'] >= 1;

    $user = new User();

    switch (isset($_GET['mode']) ?  $_GET['mode']: null) {

        case 'agregar':
            if($_SESSION['level'] < ADMIN_LEVEL){
                include(HTML_DIR . 'dashboard/users.php');
                break;
            }

            if($_POST){

                if(!isset($_POST['first_name']) || !isset($_POST['last_name']) || !isset($_POST['email']) ||
                    !isset($_POST['profile']) || !isset($_POST['password']) || !isset($_POST['password_2'])
                    || !isset($_POST['username']) || !isset($_POST['birth_date'])){
                    header('location: ?view=users');
                }
                $company_codes = array();
                if(isset($_POST['companies']) and isset($_POST['codes'])){
                    $i = 0;
                    foreach ($_POST['companies'] as $key => $value){
                        if(isset($_POST['codes'][$key]) and !empty($_POST['codes'][$key])){
                            $company_codes[$i]['company'] = $key;
                            $company_codes[$i]['code'] = $_POST['codes'][$key];
                        }

                        $i++;
                    }
                }

                if(!isset($_FILES['file']) or $_FILES['file']['size'] == 0 or $_FILES['file']['error'] == 4){
                    $user->file = '';
                }else{
                    $destiny = MEDIA_FOLDER . "users/" . slugify($_POST['username']) . "/";
                    if (!file_exists($destiny)) {
                        mkdir($destiny, 0777, true);
                    }
                    opendir($destiny);
                    $file_name = $destiny . $_FILES['file']['name'];
                    try{
                        copy($_FILES['file']['tmp_name'], $file_name);
                        $user->file = $file_name;
                    }catch (Exception $e){

                    }
                }


                $user->first_name = $_POST['first_name'];
                $user->last_name = $_POST['last_name'];
                $user->email = $_POST['email'];
                $user->profile = $_POST['profile'];
                $user->password = $_POST['password'];
                $user->username = $_POST['username'];
                $user->birth_date = date('Y-m-d', strtotime($_POST['birth_date']));
                $user->city = $_POST['city'];
                $user->street = $_POST['street'];
                $user->sector = $_POST['sector'];
                $user->home_number = $_POST['home_number'];
                $user->companies_and_codes = $company_codes;
                $user->create();
            }
            else {
                $companies = Category::get_categories();
                include(HTML_DIR . 'dashboard/users/add.user.php');
            }
            break;

        case 'editar':
            if($_SESSION['level'] >= ADMIN_LEVEL or $_GET['id'] == $_SESSION['id_user']){
                if($isset_id){
                    if($_POST){

                        if($_SESSION['level'] >= ADMIN_LEVEL){
                            $profile = $_POST['profile'];
                        }else{
                            $profile = $_SESSION['profile_id'];
                        }

                        $user->id = $_GET['id'];
                        $user->username = $_POST['username'];

                        $company_codes = array();
                        if(isset($_POST['companies']) and isset($_POST['codes'])){
                            $i = 0;
                            foreach ($_POST['companies'] as $key => $value){
                                if(isset($_POST['codes'][$key]) and !empty($_POST['codes'][$key])){
                                    $company_codes[$i]['company'] = $key;
                                    $company_codes[$i]['code'] = $_POST['codes'][$key];
                                }

                                $i++;
                            }
                        }
                        if($_FILES['file']['size'] == 0 or $_FILES['file']['error'] == 4){
                            $user->file = $user->get_file_url();

                        }else{

                            $file = $user->get_file_url();
                            try{
                                unlink($file);
                            }catch (Exception $e){

                            }
                            $destiny = MEDIA_FOLDER . "files/" . slugify($user->username) . "/";
                            if (!file_exists($destiny)) {
                                mkdir($destiny, 0777, true);
                            }
                            opendir($destiny);
                            $file_name = $destiny . $_FILES['file']['name'];
                            try{
                                copy($_FILES['file']['tmp_name'], $file_name);
                                $user->file = $file_name;
                            }catch (Exception $e){

                            }
                        }

                        $user->first_name = $_POST['first_name'];
                        $user->last_name = $_POST['last_name'];
                        $user->email = $_POST['email'];
                        $user->profile = $profile;
                        $user->password = $_POST['password'];
                        $user->birth_date = date('Y-m-d', strtotime($_POST['birth_date']));
                        $user->street = $_POST['street'];
                        $user->sector = $_POST['sector'];
                        $user->city = $_POST['city'];
                        $user->home_number = $_POST['home_number'];
                        $user->companies_and_codes = $company_codes;
                        $user->update();

                        header('location: ?view=users&mode=editar&success=true&id=' . $_GET['id']);
                    }
                    else {
                        $user->id = $_GET['id'];
                        $user_data = $user->retrieve();
                        $companies = Category::get_categories();
                        if($_SESSION['level'] >= ADMIN_LEVEL){
                            include(HTML_DIR . 'dashboard/users/edit.user.php');
                        }else{
                            include(HTML_DIR . 'agents/edit.user.php');
                        }

                    }
                } else {
                    header('location: ?view=users');
                }
            }else{
                include(HTML_DIR . 'dashboard/users.php');
            }

            break;

        case 'borrar':
            if($_SESSION['level'] < ADMIN_LEVEL){
                include(HTML_DIR . 'dashboard/users.php');
                break;
            }
            if($isset_id){
                $user->id = $_GET['id'];
                $user->delete();
                header('location: ?view=users&mode=delete&success=true');
            }
            else {
                header('location: ?view=users');
            }
            break;

        default:
            include(HTML_DIR . 'dashboard/users.php');
            break;
    }
}else {
    header('location: ?view=index');
}

