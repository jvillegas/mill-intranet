<?php

include_once 'core/models/class.User.php';
include_once 'core/models/class.Category.php';

if(isset($_SESSION['id_user'])){

    $user = new User($_SESSION['id_user']);

    switch (isset($_GET['mode']) ?  $_GET['mode']: null) {

        case 'diary_calendar':
            if (!isset($_GET['start']) || !isset($_GET['end'])) {
                die("Please provide a date range.");
            }
            $range_start = $_GET['start'] . ' 00:00:00';
            $range_end = $_GET['end'] . ' 00:00:00'; // in order to get datetime sql format

            $timezone = null;
            if (isset($_GET['timezone'])) {
                $timezone = new DateTimeZone($_GET['timezone']);
            }
            $list = $user->get_diary_calendar($range_start, $range_end);

            echo json_encode($list);
            die();
            break;

        case 'contact_list':
            $list = $user->get_contacts($_POST['limit'], $_POST['offset'], $_POST['search']);
            echo json_encode($list);
            die();
            break;
    }
    $files = Category::get_all_categories_and_files();
    require 'html/agents/agents.php';

}else{
    header('location: ?view=index');
}
