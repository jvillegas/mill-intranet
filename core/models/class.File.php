<?php
/**
 * Created by PhpStorm.
 * User: jefferson
 * Date: 28/11/18
 * Time: 09:06 AM
 */
class File{

    public $id;

    public $name;

    public $file;

    public $category;

    public $category_name;

    private $db;

    public function __construct($id=null){
        $this->db = new Connection();
        $this->id = $id;
    }

    public function create(){
        $sql = 'INSERT INTO files (name, file, category) VALUE (?, ?, ?)';
        $result = $this->db->query($sql, [
            $this->name,
            $this->file,
            $this->category
        ], 'ssi');
    }

    public function retrieve(){
        $sql = "SELECT * FROM files WHERE id = ?";
        $result = $this->db->query($sql, [
            $this->id,
        ], 'i');
        return $result->fetch_assoc();
    }

    public function update($new_file = false){
        if($new_file){

            $old_file = $this->get_file_url();
            try{
                unlink($old_file);
            }catch (Exception $e){

            }
            $destiny = MEDIA_FOLDER . "files/" . slugify($this->category_name) . "/";
            if (!file_exists($destiny)) {
                mkdir($destiny, 0777, true);
            }
            opendir($destiny);
            $file_name = $destiny . $_FILES['file']['name'];
            try{
                copy($_FILES['file']['tmp_name'], $file_name);
                $this->file = $file_name;
            }catch (Exception $e){

            }
        }else{
            $this->file = $this->get_file_url();
        }

        $sql = "UPDATE files SET name = ?, file = ?, category = ? WHERE id = ?";
        $result = $this->db->query($sql, [
            $this->name,
            $this->file,
            $this->category,
            $this->id
        ], 'ssii');
    }
    public function delete(){

        unlink($this->get_file_url());

        $sql = "DELETE FROM files WHERE id = ?";
        $this->db->query($sql, [
            $this->id,
        ], 'i');
    }

    public function get_file_url(){
        $sql = "SELECT file FROM files WHERE id = ?";
        $result = $this->db->query($sql, [
            $this->id,
        ], 'i');
        return $result->fetch_assoc()['file'];
    }

    public function get_list($limit = null, $offset = null, $order_by = null, $draw = 0){
        $list = [];

        if($limit != null and $offset != null and $order_by !=null):

            if($order_by['dir'] == 'asc'){
                $result = $this->db->query("SELECT f.*, c.name as category FROM files f 
                                                  INNER JOIN categories c on f.category = c.id
                                                  ORDER BY {$order_by['column']} ASC LIMIT ? OFFSET ?", [$limit, $offset], 'ss');
            }else{
                $result = $this->db->query("SELECT f.*, c.name as category FROM files f 
                                                  INNER JOIN categories c on f.category = c.id
                                                  ORDER BY {$order_by['column']} DESC LIMIT ? OFFSET ?", [$limit, $offset], 'ss');
            }

        elseif($limit != null and $offset != null):
            $result = $this->db->query("SELECT f.*, c.name as category FROM files f 
                                                  INNER JOIN categories c on f.category = c.id
                                                  ORDER BY name ASC LIMIT ? OFFSET ?", [$limit, $offset], 'ss');
        else:
            $result = $this->db->query("SELECT f.*, c.name as category FROM files f 
                                                  INNER JOIN categories c on f.category = c.id
                                                  ORDER BY name");
        endif;

        if($offset != null){
            $k = $offset + 1;
        }else{
            $k = 1;
        }
        $i = 0;
        $list['data'] = [];
        while($row = $result->fetch_assoc()):

            $list['data'][$i]['n'] = $k;
            $list['data'][$i]['name'] = $row['name'];
            $list['data'][$i]['file'] = str_replace('{{file}}', $row['file'], '<a href="{{file}}" target="_blank">{{file}}</a>');
            $list['data'][$i]['category'] = $row['category'];
            //$list['data'][$i]['company'] = $row['company'];
            $list['data'][$i]['edit'] = str_replace('{{id}}', $row['id'],'<div class="table-data-feature">
                                    <a href="?view=files&mode=editar&id={{id}}" class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>
                                    <a href="?view=files&mode=borrar&id={{id}}" class="item delete" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                        <i class="zmdi zmdi-delete"></i>
                                    </a>
                                </div>');


            $i++;
            $k++;
        endwhile;


        $total_records = $this->db->query('SELECT COUNT(*) FROM files');
        $total_records = $total_records->fetch_assoc();
        $list['recordsTotal'] = intval($total_records['COUNT(*)']);
        $list['recordsFiltered'] = intval($total_records['COUNT(*)']);
        $list['draw'] = intval($draw);

        return $list;
    }
}