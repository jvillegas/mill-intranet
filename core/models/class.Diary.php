<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 23/1/2019
 * Time: 2:01 PM
 */

class Diary {
    public $id;

    public $contact_date;

    public $next_visit;

    public $visit;

    public $name;

    public $company;

    public $notes;

    public $results;

    public $user;

    private $db;

    public function __construct($id = null){
        $this->db = new Connection();
        $this->id = $id;
    }

    public function create(){
        $sql = "INSERT INTO diary (contact_date, next_visit, visit, name, company, notes, results, user) VALUES
                (?, ?, ?, ?, ?, ?, ?, ?)";
        $result = $this->db->query($sql, array(
            $this->contact_date,
            $this->next_visit,
            $this->visit,
            $this->name,
            $this->company,
            $this->notes,
            $this->results,
            $this->user,
        ), 'sssssssi');

        $this->id = $this->db->insert_id;
        return $result;
    }

    public function retrieve(){
        $result = $this->db->query("SELECT * FROM diary WHERE id = ?", [$this->id], 'i');
        return $result->fetch_assoc();
    }

    public function update(){
        $result = $this->db->query(
            "UPDATE diary SET 
                    contact_date = ?, 
                    next_visit = ?, 
                    visit = ?, 
                    company = ?, 
                    notes = ?, 
                    results = ?, 
                    name = ?
                    WHERE id = ?",array(
            $this->contact_date,
            $this->next_visit,
            $this->visit,
            $this->name,
            $this->company,
            $this->notes,
            $this->results
        ), 'sssssss');

        return $result;
    }

    public function delete(){
        return $this->db->query('DELETE FROM diary WHERE id = ?', [$this->id], 'i');
    }
}