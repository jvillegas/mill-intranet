<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 23/1/2019
 * Time: 2:01 PM
 */

class Contact {
    public $id;

    public $full_name;

    public $company;

    public $phone;

    public $email;

    public $birth;

    public $user;

    private $db;

    public function __construct($id = null){
        $this->db = new Connection();
        $this->id = $id;
    }

    public function create(){
        $sql = "INSERT INTO contacts (user, full_name, company, email, phone, birth) VALUES (?, ?, ?, ?, ?, ?)";
        $result = $this->db->query($sql, array(
            $this->user,
            $this->full_name,
            $this->company,
            $this->email,
            $this->phone,
            $this->birth,
        ), 'isssss');

        $this->id = $this->db->insert_id;
        return $result;
    }

    public function retrieve(){
        $result = $this->db->query("SELECT * FROM contacts WHERE id = ?", [$this->id], 'i');
        return $result->fetch_assoc();
    }

    public function update(){
        $result = $this->db->query(
            "UPDATE contacts SET full_name = ?, email = ?, company = ?, phone = ?, birth = ? WHERE id = ?",
            [$this->full_name, $this->email, $this->company, $this->phone, $this->birth, $this->id],
            'sssssi');

        return $result;
    }

    public function delete(){
        return $this->db->query('DELETE FROM contacts WHERE id = ?', [$this->id], 'i');
    }

    public static function get_today_births(){
        $db = new Connection();
        $sql = "SELECT full_name as name, email FROM `contacts` WHERE DAY(`birth`) = DAY(CURRENT_DATE()) AND MONTH(`birth`) = MONTH(CURRENT_DATE()) and email != '' ";
        $result = $db->query($sql);
        $list = [];
        while ($row = $result->fetch_assoc()){
            $list[] = $row;
        }

        return $list;
    }
}