<?php

class Connection extends mysqli{
    public function  __construct() {
        parent::__construct(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        $this->connect_errno ? die('No se conecta D:') : null;
        $this->set_charset('utf8');
    }

    public function fetch_assoc($query){
        return mysqli_fetch_assoc($query);
    }

    function query($query, $params = NULL, $types = NULL)
    {
        if (!$params)
        {
            return parent::query($query);
        }
        $statement = $this->prepare($query);
        $types = $types ?: str_repeat('s', count($params));
        $statement->bind_param($types, ...$params);
        if($statement->execute()){
            return $statement->get_result();
        }else{
            return $statement->error;
        }

    }
}