<?php
class Category
{

    public $id;

    public $name;

    private $db;

    public function __construct($id = null)
    {
        $this->db = new Connection();
        $this->id = $id;
    }

    public function create(){
        $this->db->query("INSERT INTO categories (name) VALUES (?)", [$this->name], 's');
    }

    public function retrieve(){
        $result = $this->db->query("SELECT * FROM categories WHERE id = ?", [$this->id], 'i');
        return $result->fetch_assoc();
    }

    public function update(){
        $this->db->query("UPDATE categories SET name = ? WHERE id = ?",[$this->name, $this->id], 'si');
    }

    public function delete(){
        $this->db->query("DELETE FROM categories WHERE id = ?",[$this->id], 's');
    }

    public function get_files(){
        $sql = 'SELECT *  FROM files f WHERE category  = ?';
        $result = $this->db->query($sql, [$this->id], 'i');
        $list = [];
        while ($row = $result->fetch_assoc()) {
            $list[] = $row;
        }

        return $list;
    }

    public function get_list($limit = null, $offset = null, $order_by = null, $draw = 0){
        $list = [];

        if($limit != null and $offset != null and $order_by !=null):

            if($order_by['dir'] == 'asc'){
                $result = $this->db->query("SELECT * FROM categories ORDER BY {$order_by['column']} ASC LIMIT ? OFFSET ?", [$limit, $offset], 'ss');
            }else{
                $result = $this->db->query("SELECT * FROM categories ORDER BY {$order_by['column']} DESC LIMIT ? OFFSET ?", [$limit, $offset], 'ss');
            }

        elseif($limit != null and $offset != null):
            $result = $this->db->query("SELECT * FROM categories ORDER BY name ASC LIMIT ? OFFSET ?", [$limit, $offset], 'ss');
        else:
            $result = $this->db->query("SELECT * FROM categories ORDER BY name");
        endif;

        if($offset != null){
            $k = $offset + 1;
        }else{
            $k = 1;
        }
        $i = 0;
        $list['data'] = [];
        while($row = $result->fetch_assoc()):

            $list['data'][$i]['n'] = $k;
            $list['data'][$i]['name'] = $row['name'];
            $list['data'][$i]['edit'] = str_replace('{{id}}', $row['id'],'<div class="table-data-feature">
                                    <a href="?view=categories&mode=editar&id={{id}}" class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>
                                    <a href="?view=categories&mode=borrar&id={{id}}" class="item delete" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                                        <i class="zmdi zmdi-delete"></i>
                                    </a>
                                </div>');


            $i++;
            $k++;
        endwhile;


        $total_records = $this->db->query('SELECT COUNT(*) FROM categories');
        $total_records = $total_records->fetch_assoc();
        $list['recordsTotal'] = intval($total_records['COUNT(*)']);
        $list['recordsFiltered'] = intval($total_records['COUNT(*)']);
        $list['draw'] = intval($draw);

        return $list;
    }

    public static function  get_all_categories_and_files(){
        $sql = 'SELECT c.name as category, c.id as category_id, f.name as file_name, f.file as file_url, f.id as file_id FROM files f INNER JOIN categories c ON c.id = f.category';
        $db = new Connection();
        $result = $db->query($sql);
        $list = [];
        while($row = $result->fetch_assoc()){
            $list[$row['category_id']]['name'] = $row['category'];
            $list[$row['category_id']]['category_id'] = $row['category_id'];
            $list[$row['category_id']]['files'][] = array(
                'name' => $row['file_name'],
                'file' => $row['file_url']
            );
        }

        return $list;
    }

    public static function get_categories(){
        $sql = 'SELECT * FROM categories';
        $db = new Connection();
        $result = $db->query($sql);
        $list = [];
        while($row = $result->fetch_assoc()){
            $list[] = $row;
        }

        return $list;
    }
}
