<?php 
class Company{

	public $id;

    public $name;

    private $db;

    public function __construct($id=null){
        $this->db = new Connection();
        $this->id = $id;
    }

    public function get_categories_and_files(){
    	$sql = 'SELECT c.name as category, c.id as category_id, f.name as file_name, f.file as file_url, f.id as file_id FROM files f INNER JOIN categories c ON c.id = f.category WHERE c.company = ?';
    	$result = $this->db->query($sql, [$this->id], 'i');
    	$list = [];
    	while($row = $result->fetch_assoc()){
    		$list[$row['category_id']]['name'] = $row['category'];
    		$list[$row['category_id']]['files'][] = array(
    			'name' => $row['file_name'],
    			'file' => $row['file_url']
    		);
    	}

    	return $list;
    }

    public static function get_companies(){
        $sql = "SELECT * FROM companies";
        $db = new Connection();
        $result = $db->query($sql);
        $list = [];

        while ($row = $result->fetch_assoc()){
            $list[] = $row;
        }
        return $list;
    }
}

