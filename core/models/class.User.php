<?php
/**
 * Created by PhpStorm.
 * User: jefferson
 * Date: 27/11/18
 * Time: 08:44 AM
 */

class User{

    public $id;

    public $username;

    public $first_name;

    public $last_name;

    public $email;

    public $password;

    public $profile;

    public $birth_date;

    public $city;

    public $street;

    public $sector;

    public $home_number;

    public $sender;

    public $companies_and_codes = array();

    public $file;

    private $db;

    const ADMIN_LEVEL = 2;
    const PROFILE_LEVEL = 1;

    public function __construct($id=null){
        $this->db = new Connection();
        $this->id = $id;
    }

    public function create(){
        $sql= "INSERT INTO users
    (username, password, first_name, last_name, email, profile, birth_date, street, city, home_number, sector, file)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $result = $this->db->query($sql, array(
            $this->username,
            Encrypt($this->password),
            $this->first_name,
            $this->last_name,
            $this->email,
            $this->profile,
            $this->birth_date,
            $this->street,
            $this->city,
            $this->home_number,
            $this->sector,
            $this->file
        ), 'ssssssssssss');

        $this->id = $this->db->insert_id;
        $this->add_companies();
        header('location: ?view=users&mode=agregar&success=true');
    }

    public function add_companies(){
        $sql = "INSERT INTO categories_users (user, category, code) VALUES (?, ? ,?)";
        $query = $this->db->prepare($sql);
        $query->bind_param('iis',$this->id, $company, $code);
        foreach($this->companies_and_codes as $company_and_code){
            $company = $company_and_code['company'];
            $code = $company_and_code['code'];
            if(!$query->execute()){
                var_dump($query->error);
                die();
            }
        }
    }

    public function delete(){
        $sql = 'DELETE FROM users WHERE id = ?';

        return $this->db->query($sql, [$this->id], 'i');
    }

    public function retrieve(){
        $sql = 'SELECT * FROM users WHERE id = ?';

        $result = $this->db->query($sql, [$this->id], 'i');
        $user =  $result->fetch_assoc();

        $sql = 'SELECT category, code FROM categories_users WHERE user = ?';
        $result = $this->db->query($sql, [$this->id], 'i');
        $list = [];
        while ($row = $result->fetch_assoc()){
            $list[] = $row;
        }
        $user['companies_and_codes'] = $list;
        return $user;
    }

    public function update_companies(){
        $this->db->query("DELETE FROM categories_users WHERE user = ?", [$this->id], 'i');

        $this->add_companies();
    }

    public function update(){
        if(!empty($this->password)){
            $sql = 'UPDATE users SET  username = ?, password = ?, first_name = ?, last_name = ?, email = ? , profile = ? ,
      sector = ? , street = ? , city = ? , file = ? , birth_date = ? , home_number = ?
      WHERE id = ?';
            $this->update_companies();
            return $this->db->query($sql, array(
                $this->username,
                Encrypt($this->password),
                $this->first_name,
                $this->last_name,
                $this->email,
                $this->profile,
                $this->sector,
                $this->street,
                $this->city,
                $this->file,
                $this->birth_date,
                $this->home_number,
                $this->id
            ), 'sssssssssssi');
        }else{
            $sql = 'UPDATE users SET  username = ?, first_name = ?, last_name = ?, email = ? , profile = ? ,
      sector = ? , street = ? , city = ? , file = ? , birth_date = ? , home_number = ?
      WHERE id = ?';
            $this->update_companies();
            return $this->db->query($sql, array(
                $this->username,
                $this->first_name,
                $this->last_name,
                $this->email,
                $this->profile,
                $this->sector,
                $this->street,
                $this->city,
                $this->file,
                $this->birth_date,
                $this->home_number,
                $this->id,
            ), 'sssssssssssi');
        }

    }



    public function get_file_url(){
        $sql = "SELECT file FROM users WHERE id = ?";

        $result = $this->db->query($sql, [$this->id], 'i');

        $response = $result->fetch_assoc();
        return $response['file'];
    }

    public function get_users($limit = null, $offset = null, $order_by = null, $draw = 0){
        $users = [];

        if($limit != null and $offset != null and $order_by !=null):

            if($order_by['dir'] == 'asc'){
                $result = $this->db->query("SELECT u.id, CONCAT(u.first_name, ' ', u.last_name) as full_name, u.email ,u.username, p.name as profile, p.level FROM users u
        INNER JOIN profiles p on u.profile = p.id ORDER BY {$order_by['column']} ASC LIMIT ? OFFSET ?", [$limit, $offset], 'ss');
            }else{
                $result = $this->db->query("SELECT u.id, CONCAT(u.first_name, ' ', u.last_name) as full_name, u.email ,u.username, p.name as profile, p.level FROM users u
        INNER JOIN profiles p on u.profile = p.id ORDER BY {$order_by['column']} DESC LIMIT ? OFFSET ?", [$limit, $offset], 'ss');
            }

        elseif($limit != null and $offset != null):
            $result = $this->db->query("SELECT u.id, CONCAT(u.first_name, ' ', u.last_name) as full_name, u.email ,u.username, p.name as profile, p.level FROM users u
        INNER JOIN profiles p on u.profile = p.id ORDER BY full_name ASC LIMIT ? OFFSET ?", [$limit, $offset], 'ss');
        else:
            $result = $this->db->query("SELECT u.id, CONCAT(u.first_name, ' ', u.last_name) as full_name, u.email ,u.username, p.name as profile, p.level FROM users u
        INNER JOIN profiles p on u.profile = p.id ORDER BY full_name");
        endif;

        if($offset != null){
            $k = $offset + 1;
        }else{
            $k = 1;
        }
        $i = 0;
        $users['data'] = [];
        while($row = $result->fetch_assoc()):

            $users['data'][$i]['n'] = $k;
            $users['data'][$i]['full_name'] = $row['full_name'];
            $users['data'][$i]['username'] = $row['username'];
            $users['data'][$i]['email'] = $row['email'];

            if($row['level'] >= self::ADMIN_LEVEL){
                $users['data'][$i]['profile'] = str_replace('{{profile}}',$row['profile'],'<span class="role admin">{{profile}}</span>');
            }else{
                $users['data'][$i]['profile'] = str_replace('{{profile}}',$row['profile'],'<span class="role user">{{profile}}</span>');
            }


            if($row['id'] != $this->sender){
                $users['data'][$i]['edit'] = str_replace('{{id}}', $row['id'],'<div class="table-data-feature">
          <a href="?view=sales&user={{id}}" class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ventas">
          <i class="zmdi zmdi-file-plus"></i>
          </a>
          <a href="?view=users&mode=editar&id={{id}}" class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
          <i class="zmdi zmdi-edit"></i>
          </a>
          <a href="?view=users&mode=borrar&id={{id}}" class="item delete" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
          <i class="zmdi zmdi-delete"></i>
          </a>
          </div>');
            }else{
                $users['data'][$i]['edit'] = str_replace('{{id}}', $row['id'],'<div class="table-data-feature">
          <a href="?view=users&mode=editar&id={{id}}" class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
          <i class="zmdi zmdi-edit"></i>
          </a>
          </div>');
            }

            $i++;
            $k++;
        endwhile;


        $total_records = $this->db->query('SELECT COUNT(*) FROM users');
        $total_records = $total_records->fetch_assoc();
        $users['recordsTotal'] = intval($total_records['COUNT(*)']);
        $users['recordsFiltered'] = intval($total_records['COUNT(*)']);
        $users['draw'] = intval($draw);

        return $users;
    }

    public static function get_today_births(){
        $db = new Connection();
        $sql = "SELECT CONCAT(first_name, ' ', last_name) as name, email, username FROM `users` WHERE DAY(`birth_date`) = DAY(CURRENT_DATE()) AND MONTH(`birth_date`) = MONTH(CURRENT_DATE())";
        $result = $db->query($sql);
        $list = [];
        while ($row = $result->fetch_assoc()){
            $list[] = $row;
        }

        return $list;
    }

    public static function get_admins(){
        $db = new Connection();
        $sql = "SELECT CONCAT(users.first_name, ' ', users.last_name) as name, users.email FROM users
      INNER JOIN profiles p on users.profile = p.id WHERE p.level = 2";
        $result = $db->query($sql);
        $list = [];
        while ($row = $result->fetch_assoc()){
            $list[] = $row;
        }
        return $list;
    }

    public static function username_exists($username){
        $db =  new Connection();
        $sql = "SELECT users.username FROM users WHERE username = ?";

        $result = $db->query($sql, [$username], 's')->fetch_assoc();

        if($result ==  null){
            return false;
        }else{
            return true;
        }
    }

    public function get_contact_list($limit = null, $offset = null, $order_by = null, $draw = 0){

        $list = [];

        if($limit != null and $offset != null and $order_by !=null):

            if($order_by['dir'] == 'asc'){
                $result = $this->db->query(
                    "SELECT * FROM contacts WHERE user = ? ORDER BY {$order_by['column']} ASC LIMIT ? OFFSET ?",
                    [$this->id, $limit, $offset], 'iss');
            }else{
                $result = $this->db->query(
                    "SELECT * FROM contacts WHERE user = ? ORDER BY {$order_by['column']} DESC LIMIT ? OFFSET ?",
                    [$this->id, $limit, $offset], 'iss');
            }

        elseif($limit != null and $offset != null):
            $result = $this->db->query(
                "SELECT * FROM contacts WHERE user = ? ORDER BY full_name LIMTT ? OFFSET ?", [$this->id, $limit, $offset], 'iss');
        else:
            $result = $this->db->query(
                "SELECT * FROM contacts WHERE user = ? ORDER BY full_name", [$this->id], 'i');
        endif;

        if($offset != null){
            $k = $offset + 1;
        }else{
            $k = 1;
        }
        $i = 0;
        $list['data'] = [];
        while($row = $result->fetch_assoc()):
            $list['data'][$i] = $row;
            $list['data'][$i]['n'] = $k;
            $list['data'][$i]['edit'] = str_replace('{{id}}', $row['id'],'<div class="table-data-feature">
                  <a href="?view=contacts&mode=editar&id={{id}}" class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
                  <i class="zmdi zmdi-edit"></i>
                  </a>
                  <a href="?view=contacts&mode=borrar&id={{id}}" class="item delete" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                  <i class="zmdi zmdi-delete"></i>
                  </a>
                  </div>');
            $i++;
            $k++;
        endwhile;


        $total_records = $this->db->query('SELECT COUNT(*) FROM contacts WHERE user = ?',[$this->id], 'i' );
        $total_records = $total_records->fetch_assoc();
        $list['recordsTotal'] = intval($total_records['COUNT(*)']);
        $list['recordsFiltered'] = intval($total_records['COUNT(*)']);
        $list['draw'] = intval($draw);

        return $list;
    }

    public function get_contacts($limit = null, $offset = null, $search = ''){

        $list = [];
        if(($limit != null and $offset != null and $search)):
            $search = '%' . $search . '%';
            $result = $this->db->query(
                "SELECT * FROM `contacts` WHERE `user` = ? 
                        AND ( full_name LIKE ? OR company LIKE ? OR email LIKE ? OR phone LIKE ? )
                        ORDER BY `full_name` ASC LIMIT ? OFFSET ?",
                [$this->id, $search, $search, $search, $search, $limit, $offset], 'issssss');

        elseif($limit != null and $offset != null):
            $result = $this->db->query(
                "SELECT * FROM `contacts` WHERE `user` = ? ORDER BY `full_name` ASC LIMIT ? OFFSET ?", [$this->id, $limit, $offset], 'iss');

        else:
            $result = $this->db->query(
                "SELECT * FROM contacts WHERE user = ? ORDER BY full_name", [$this->id], 'i');
        endif;


        $i = 0;
        while($row = $result->fetch_assoc()):
            $row['birth'] = change_date_format_to_front($row['birth']);
            $list[] = $row;


            $i++;
        endwhile;

        return $list;
    }

    public function get_diary($limit = null, $offset = null, $order_by = null, $draw = 0){

        $list = [];

        if($limit != null and $offset != null and $order_by !=null):

            if($order_by['dir'] == 'asc'){
                $result = $this->db->query(
                    "SELECT * FROM diary WHERE user = ? ORDER BY {$order_by['column']} ASC LIMIT ? OFFSET ?",
                    [$this->id, $limit, $offset], 'iss');
            }else{
                $result = $this->db->query(
                    "SELECT * FROM diary WHERE user = ? ORDER BY {$order_by['column']} DESC LIMIT ? OFFSET ?",
                    [$this->id, $limit, $offset], 'iss');
            }

        elseif($limit != null and $offset != null):
            $result = $this->db->query(
                "SELECT * FROM diary WHERE user = ? ORDER BY name LIMTT ? OFFSET ?", [$this->id, $limit, $offset], 'iss');
        else:
            $result = $this->db->query(
                "SELECT * FROM diary WHERE user = ? ORDER BY name", [$this->id], 'i');
        endif;

        if($offset != null){
            $k = $offset + 1;
        }else{
            $k = 1;
        }
        $i = 0;
        $list['data'] = [];
        while($row = $result->fetch_assoc()):
            $list['data'][$i] = $row;
            $list['data'][$i]['n'] = $k;
            $list['data'][$i]['edit'] = str_replace('{{id}}', $row['id'],'<div class="table-data-feature">
                              <a href="?view=diary&mode=editar&id={{id}}" class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver / Editar">
                              <i class="zmdi zmdi-edit"></i>
                              </a>
                              <a href="?view=diary&mode=borrar&id={{id}}" class="item delete" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar">
                              <i class="zmdi zmdi-delete"></i>
                              </a>
                              </div>');
            $i++;
            $k++;
        endwhile;


        $total_records = $this->db->query('SELECT COUNT(*) FROM diary WHERE user = ?',[$this->id], 'i' );
        $total_records = $total_records->fetch_assoc();
        $list['recordsTotal'] = intval($total_records['COUNT(*)']);
        $list['recordsFiltered'] = intval($total_records['COUNT(*)']);
        $list['draw'] = intval($draw);

        return $list;
    }

    public function get_diary_calendar($range_start, $range_end){
        $list = [];

        $sql = "SELECT id, CONCAT('Visita: ', name , ' - ', company) as title,
                next_visit as start, notes, results, contact_date, next_visit, visit, name, company FROM `diary`
                            WHERE (next_visit BETWEEN ? and ?) and user = ?";
        $result = $this->db->query($sql, [$range_start, $range_end, $this->id], 'ssi');

        $i = 0;
        while($row = $result->fetch_assoc()){
            $list[$i] = $row;
            $list[$i]['start'] = format_date_to_fullcalendar($row['start']);
            $list[$i]['contact_date'] = change_date_format_to_front($row['contact_date']);
            $list[$i]['visit'] = change_datetime_format_to_front($row['visit']);
            $list[$i]['next_visit'] = change_datetime_format_to_front($row['next_visit']);
            $list[$i]['type'] = 'visit';
            $i++;
        }

        $sql = "SELECT id, CONCAT('Cumpleaños: ', full_name) as title,
                company, birth, email, phone FROM `contacts`
                            WHERE (birth BETWEEN ? and ?) and user = ?";
        $result = $this->db->query($sql, [$range_start, $range_end, $this->id], 'ssi');

        while($row = $result->fetch_assoc()){
            $list[$i] = $row;
            $list[$i]['start'] = format_date_to_fullcalendar($row['birth']);
            $list[$i]['type'] = 'contact';
            $list[$i]['allDay'] = true;
            $list[$i]['backgroundColor'] = '#28a745';

            $i++;
        }
        return $list;
    }
}
