<?php
/**
 * Created by PhpStorm.
 * User: jefferson
 * Date: 05/12/18
 * Time: 09:34 AM
 */

class Sale {

    public $id;

    public $date;

    public $clients;

    public $company;

    public $policy_number;

    public $business_type;

    public $transaction_number;

    public $payment_method;

    public $amount;

    public $first_commissioner;

    public $notes;

    public $user;

    public $user_profile;

    public $pay_button = '<span style="cursor: pointer" data-pay="{{id}}" class="item pay_button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cambiar a pagada">
                                        <i class="far fa-check-square"></i>
                                    </span>';

    public $edit_button = '<a href="?view=sales&mode=editar&id={{id}}" class="item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>';

    public $refund_button = ' <span style="cursor: pointer" data-pay="{{id}}" class="item refund_button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cambiar a sin pagar">
                                        <i class="far fa-minus-square"></i>
                                    </span>';

    public $view_button = '<a data-sale="{{id}}" href="#" data-target="#view_sale" data-toggle="modal" class="item view_sale_link" data-placement="top" title="" data-original-title="Ver detalles">
                                        <i class="zmdi zmdi-eye" data-toggle="tooltip" data-placement="top" title="" data-original-title="Ver detalles"></i>
                                    </a>';

    public $delete_button = ' <a href="?view=sales&mode=borrar&id={{id}}" data-toggle="confirmation" data-title="¿Eliminar Venta?" class="item delete" data-placement="top" title="" data-original-title="Eliminar">
                                        <i class="zmdi zmdi-delete"></i>
                                    </a>';

    private $db;

    public function __construct($id = null) {
        $this->id = $id;
        $this->db = new Connection();
    }

    public function create(){
        $sql = 'INSERT INTO sales (date, clients, company, policy_number, business_type, transaction_number, payment_method, amount, first_commissioner, user, notes) 
                VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        $result = $this->db->query($sql, [
            $this->date,
            $this->clients,
            $this->company,
            $this->policy_number,
            $this->business_type,
            $this->transaction_number,
            $this->payment_method,
            $this->amount,
            $this->first_commissioner,
            $this->user,
            $this->notes
        ], 'sssssssiiis');
        $this->id = $this->db->insert_id;
        return $result;
    }

    public function retrieve(){
        $result = $this->db->query("SELECT * FROM sales WHERE id = ?", [$this->id], 'i');
        return $result->fetch_assoc();
    }

    public function delete(){
        $sql = "DELETE FROM sales WHERE id = ?";
        $result = $this->db->query($sql, [$this->id], 'i');
        return $result;
    }

    public function pay(){
        $sql = 'UPDATE sales SET is_paid = 1 WHERE id = ?';

        $statement = $this->db->prepare($sql);
        $statement->bind_param('i', $this->id);
        if($statement->execute()){
            return array(
                'response' => 'success',
                'data' => $statement->get_result()
            );
        }else{
            return array(
                'response' => 'error',
                'msg' => $statement->error
            );
        }

    }

    public function refund(){
        $sql = 'UPDATE sales SET is_paid = 0 WHERE id = ?';

        $statement = $this->db->prepare($sql);
        $statement->bind_param('i', $this->id);
        if($statement->execute()){
            return array(
                'response' => 'success',
                'data' => $statement->get_result()
            );
        }else{
            return array(
                'response' => 'error',
                'msg' => $statement->error
            );
        }

    }

    public function get_list($limit = null, $offset = null, $order_by = null, $draw = 0){
        $list = [];

        if($this->user == -1){
            $base_sql = "SELECT sales.*,CONCAT(u.first_name, ' ', u.last_name) as username FROM sales INNER JOIN users u on sales.user = u.id ";
            $params = [$limit, $offset];
            $types = 'ss';
            $total_records = $this->db->query('SELECT COUNT(*) FROM sales');
            $total_records = $total_records->fetch_assoc();
        }else{
            $base_sql = "SELECT sales.*,CONCAT(u.first_name, ' ', u.last_name) as username FROM sales INNER JOIN users u on sales.user = u.id WHERE user = ? ";
            $params = [$this->user, $limit, $offset];
            $types = 'iss';
            $total_records = $this->db->query('SELECT COUNT(*) FROM sales WHERE user = ?', [$this->user], 'i');
            $total_records = $total_records->fetch_assoc();
        }

        if($limit != null and $offset != null and $order_by !=null):

            if($order_by['dir'] == 'asc'){
                $result = $this->db->query($base_sql . " ORDER BY {$order_by['column']} ASC LIMIT ? OFFSET ?", $params, $types);
            }else{
                $result = $this->db->query($base_sql . " ORDER BY {$order_by['column']} DESC LIMIT ? OFFSET ?", $params, $types);
            }

        elseif($limit != null and $offset != null):
            $result = $this->db->query($base_sql . " ORDER BY name ASC LIMIT ? OFFSET ?", $params, $types);
        else:
            $result = $this->db->query($base_sql . " ORDER BY name");
        endif;

        if($offset != null){
            $k = $offset + 1;
        }else{
            $k = 1;
        }
        $i = 0;
        $list['data'] = [];
        while($row = $result->fetch_assoc()):
            $list['data'][$i] = $row;
            $list['data'][$i]['n'] = $k;
            $list['data'][$i]['commission'] = ($list['data'][$i]['amount'] * $list['data'][$i]['first_commissioner'])/100;
            $list['data'][$i]['date'] = change_date_format($list['data'][$i]['date']);

            $list['data'][$i]['user'] = str_replace('{{name}}', $row['username'], '<a href="?view=sales&user={{id}}">{{name}}</a>');
            $list['data'][$i]['user'] = str_replace('{{id}}', $row['user'], $list['data'][$i]['user']);


            if($row['is_paid'] == 0 and $this->user_profile >= ADMIN_LEVEL){
                $list['data'][$i]['edit'] = str_replace('{{id}}', $row['id'],
                    '<div class="table-data-feature">'
                    . $this->pay_button
                    . $this->view_button
                    . $this->delete_button
                    .'</div>');

            }elseif($row['is_paid'] == 1 and $this->user_profile >= ADMIN_LEVEL){
                $list['data'][$i]['edit'] = str_replace('{{id}}', $row['id'],
                    '<div class="table-data-feature">'
                    . $this->view_button
                    . $this->refund_button
                    .'</div>');
            }else{
                $list['data'][$i]['edit'] = str_replace('{{id}}', $row['id'],
                    '<div class="table-data-feature">'
                    . $this->view_button
                    . $this->delete_button
                    .'</div>');
            }


            $i++;
            $k++;
        endwhile;


        $list['recordsTotal'] = intval($total_records['COUNT(*)']);
        $list['recordsFiltered'] = intval($total_records['COUNT(*)']);
        $list['draw'] = intval($draw);

        return $list;
    }
}