<?php
include_once 'core/models/class.Sale.php';

if(!isset($_SESSION['id_user'])) {
    echo json_encode(array(
        'response' => 'error',
        'msg' =>_('No se ha iniciado sesión')));
    die();
}
$order_by = null;
if(isset($_POST['order'])){
    $order_by['dir'] = $_POST['order'][0]['dir'];
    switch ($_POST['order'][0]['column']){
        case 1:
            $order_by['column'] = 'username';
        case 2:
            $order_by['column'] = 'date';
            break;
        case 3:
            $order_by['column'] = 'clients';
            break;
        case 4:
            $order_by['column'] = 'company';
            break;
        case 5:
            $order_by['column'] = 'policy_number';
            break;
        case 6:
            $order_by['column'] = 'business_type';
            break;
        case 7:
            $order_by['column'] = 'transaction';
            break;
        case 8:
            $order_by['column'] = 'payment_method';
            break;
        case 9:
            $order_by['column'] = 'is_paid';
            break;

    }
}

$sale = new Sale();
$sale->user_profile = $_SESSION['level'];
if($_SESSION['level'] >= ADMIN_LEVEL){
    if(isset($_POST['user']) and !empty($_POST['user'])){
        $sale->user = $_POST['user'];
    }else{
        $sale->user = '-1';
    }

}else{
    $sale->user = $_SESSION['id_user'];
}

$list = $sale->get_list($_POST['length'], $_POST['start'], $order_by, $_POST['draw']);

echo json_encode($list);