<?php
include_once 'core/models/class.Sale.php';

if(!isset($_SESSION['id_user']) or $_SESSION['level'] < ADMIN_LEVEL or !isset($_POST['id'])) {
    echo json_encode(array(
        'response' => 'error',
        'msg' =>_('Usted no puede ver esto')));
    die();
}
$sale = new Sale($_POST['id']);
$result = $sale->pay();

echo json_encode($result);
die();
