<?php
include_once 'core/models/class.User.php';
if(!isset($_SESSION['id_user'])) {
    echo json_encode(array(
        'response' => 'error',
        'msg' =>_('No se ha iniciado sesión')));
    die();
}

$user = new User($_SESSION['id_user']);
if(isset($_GET['calendar']) and $_GET['calendar'] == true){

    if (!isset($_GET['start']) || !isset($_GET['end'])) {
        die("Please provide a date range.");
    }
    $range_start = parseDateTime($_GET['start']);
    $range_end = parseDateTime($_GET['end']);

    $timezone = null;
    if (isset($_GET['timezone'])) {
        $timezone = new DateTimeZone($_GET['timezone']);
    }
    $list = $user->get_diary_calendar();

    echo json_encode($list);
}else{
    $order_by = null;
    if(isset($_POST['order'])){
        $order_by['dir'] = $_POST['order'][0]['dir'];
        switch ($_POST['order'][0]['column']){
            case 1:
                $order_by['column'] = 'name';
                break;
            case 2:
                $order_by['column'] = 'company';
                break;
            case 3:
                $order_by['column'] = 'visit';
                break;
            case 4:
                $order_by['column'] = 'next_visit';
                break;

        }
    }


    $list = $user->get_diary($_POST['length'], $_POST['start'], $order_by, $_POST['draw']);

    echo json_encode($list);
}
die();