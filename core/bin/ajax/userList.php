<?php
include_once 'core/models/class.User.php';

$order_by = null;
if(isset($_POST['order'])){
    $order_by['dir'] = $_POST['order'][0]['dir'];
    switch ($_POST['order'][0]['column']){
        case 1:
            $order_by['column'] = 'username';
            break;
        case 2:
            $order_by['column'] = 'full_name';
            break;
        case 3:
            $order_by['column'] = 'email';
            break;
        case 4:
            $order_by['column'] = 'profile';
            break;

    }
}

$user = new User();
$user->sender = $_SESSION['id_user'];
$users_list = $user->get_users($_POST['length'], $_POST['start'], $order_by, $_POST['draw']);

echo json_encode($users_list);