<?php
include_once 'core/models/class.Sale.php';

if(isset($_SESSION['id_user']) and isset($_POST['sale_id'])){
    $sale = new Sale($_POST['sale_id']);
    $response = $sale->retrieve();
    if(!empty($response)){
        echo json_encode(array(
            'response' => 'success',
            'sale' => $response
        ));
    }else{
        echo json_encode(array(
            'response' => 'error',
            'msg' => 'no se ha encontrado la venta'
        ));
    }
}