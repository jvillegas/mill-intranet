<?php
include_once 'core/models/class.File.php';

$order_by = null;
if(isset($_POST['order'])){
    $order_by['dir'] = $_POST['order'][0]['dir'];
    switch ($_POST['order'][0]['column']){
        case 1:
            $order_by['column'] = 'name';
            break;
        case 2:
            $order_by['column'] = 'file';
            break;
        case 3:
            $order_by['column'] = 'group';
            break;

    }
}

$file = new File();
$list = $file->get_list($_POST['length'], $_POST['start'], $order_by, $_POST['draw']);

echo json_encode($list);