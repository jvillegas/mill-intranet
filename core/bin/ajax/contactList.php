<?php
include_once 'core/models/class.User.php';
if(!isset($_SESSION['id_user'])) {
    echo json_encode(array(
        'response' => 'error',
        'msg' =>_('No se ha iniciado sesión')));
    die();
}
$order_by = null;
if(isset($_POST['order'])){
    $order_by['dir'] = $_POST['order'][0]['dir'];
    switch ($_POST['order'][0]['column']){
        case 1:
            $order_by['column'] = 'full_name';
            break;
        case 2:
            $order_by['column'] = 'company';
            break;
        case 3:
            $order_by['column'] = 'email';
            break;
        case 4:
            $order_by['column'] = 'phone';
            break;

    }
}

$user = new User($_SESSION['id_user']);
$list = $user->get_contact_list($_POST['length'], $_POST['start'], $order_by, $_POST['draw']);

echo json_encode($list);