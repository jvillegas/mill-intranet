<?php

if(isset($_POST['username']) and isset($_POST['password']) and !empty($_POST['username']) and !empty($_POST['password'])) {


  $db = new Connection();

  $user = $db->real_escape_string($_POST['username']);
  
  $password = Encrypt($_POST['password']);

  $sql = $db->query("SELECT u.id, u.first_name, u.last_name, CONCAT(u.first_name, ' ', u.last_name) as full_name, u.email, p.level, p.name as profile, p.id as profile_id FROM users u
  INNER JOIN profiles p on u.profile = p.id
  WHERE username = ? AND password = ?", [$user, $password], 'ss');

  if ($sql->num_rows > 0) {
    $result = $sql->fetch_assoc();

    $_SESSION['id_user'] = $result['id'];
    $_SESSION['first_name'] = $result['first_name'];
    $_SESSION['last_name'] = $result['last_name'];
    $_SESSION['full_name'] = $result['full_name'];
    $_SESSION['email'] = $result['email'];
    $_SESSION['level'] = $result['level'];
    $_SESSION['profile'] = $result['profile'];
    $_SESSION['profile_id'] = $result['profile_id'];

    if (session_status() == PHP_SESSION_NONE) {
      ini_set('session.cookie_lifetime', time() + (60*60*24));
    }

    echo '{"response": "success", "msg": "Iniciando..."}';

  }else{
    echo json_encode(array(
      'response' => 'error',
      'msg' =>_('Usuario o contraseña incorrectos')));
      die();
    }
  }else{
    echo json_encode(array(
      'response' => 'error',
      'msg' =>_('No se han ingresado todos los datos')));
      die();
    }

    $db->close();
