<?php
set_include_path('/home/millfina/public_html/admin/');
include ('core/core.php');
include ('core/models/class.User.php');

$admins = User::get_admins();

$today_births = User::get_today_births();
var_dump($today_births);
if(!empty($today_births)){
    $html = '';

    foreach ($today_births as $birth){
        $html .= $birth['name'] . ' - ' . $birth['username'] . ' - ' . $birth['email'] . "\n";
    }

    /**
     * This example shows sending a message using PHP's mail() function.
     */


//Create a new PHPMailer instance
    $mail = new PHPMailer;
//Tell PHPMailer to use SMTP
    $mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
    $mail->SMTPDebug = 2;
//Ask for HTML-friendly debug output
    $mail->Debugoutput = 'html';
//Set the hostname of the mail server
    $mail->Host = SMTP_HOST;
//Set the SMTP port number - likely to be 25, 465 or 587
    $mail->Port = SMTP_PORT;
    $mail->CharSet = 'UTF-8';
//Whether to use SMTP authentication
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'ssl';
//Username to use for SMTP authentication
    $mail->Username = SMTP_EMAIL;
//Password to use for SMTP authentication
    $mail->Password = SMTP_PASS;
//Set who the message is to be sent from
    $mail->setFrom(SMTP_EMAIL, REPLY_NAME);
//Set an alternative reply-to address
    $mail->addReplyTo(REPLY_EMAIL, REPLY_NAME);
//Set who the message is to be sent to
    foreach ($admins as $admin){
        $mail->AddAddress($admin['email'], $admin['name']);
    }
//Set the subject line
    $mail->Subject = 'Mill Financial Group | Cumpleaños del día';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
    $mail->msgHTML($html);
//Replace the plain text body with one created manually
    $mail->AltBody = 'This is a plain-text message body';

//send the message, check for errors
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Message sent!";
    }

}
