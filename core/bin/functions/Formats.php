<?php
/**
 * Created by PhpStorm.
 * User: jefferson
 * Date: 10/12/18
 * Time: 09:39 AM
 */
function change_date_format($date){
    $new_date = date("d/m/Y", strtotime($date));
    return $new_date;
}

function change_date_format_to_db($date){
    $format = "d/m/Y";
    $dateobj = DateTime::createFromFormat($format, $date);
    return $dateobj->format('Y-m-d');
}

/**
 * Receive the date in the inputs format from the Front-end
 * and changes it to database sql format
 *
 * @param $date string
 * @return string
 */
function change_datetime_format($date){
    $format = "d/m/Y h:i a";
    $dateobj = DateTime::createFromFormat($format, $date);
    return $dateobj->format('Y-m-d H:i:s');
}

function change_datetime_format_to_front($date){
    $format = "Y-m-d H:i:s";
    $dateobj = DateTime::createFromFormat($format, $date);
    return $dateobj->format('d/m/Y h:i a');
}

function change_date_format_to_front($date){
    if(!$date){
        return '';
    }
    $format = "Y-m-d";
    $dateobj = DateTime::createFromFormat($format, $date);
    return $dateobj->format('d/m/Y');
}

function parseDateTime($string, $timezone=null) {
    $date = new DateTime(
        $string,
        $timezone ? $timezone : new DateTimeZone('UTC')
    // Used only when the string is ambiguous.
    // Ignored if string has a timezone offset in it.
    );
    if ($timezone) {
        // If our timezone was ignored above, force it.
        $date->setTimezone($timezone);
    }
    return $date;
}

function stripTime($datetime) {
    return new DateTime($datetime->format('Y-m-d'));
}

function format_date_to_fullcalendar($string, $timezone=null){
    $date = new DateTime(
        $string,
        $timezone ? $timezone : new DateTimeZone('UTC')
    // Used only when the string is ambiguous.
    // Ignored if string has a timezone offset in it.
    );
    if ($timezone) {
        // If our timezone was ignored above, force it.
        $date->setTimezone($timezone);
    }
    $first_part = $date->format("Y-m-d");
    $second_part = $date->format("H:i:sO");
    return $first_part . "T". $second_part;
}