<?php
function success_msg($msg){
    echo '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">'
        . '<span class="badge badge-pill badge-success">Muy bien!</span>  '
        . ' '. $msg
        .'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
        .'	<span aria-hidden="true">×</span>'
        .'</button>'
        .'</div>';
}

function error_msg($msg){
    echo '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">'
        . '<span class="badge badge-pill badge-danger">Oh no!</span>  '
        . ' '. $msg
        .'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
        .'	<span aria-hidden="true">×</span>'
        .'</button>'
        .'</div>';
}