<?php
function get_file_icon($ext, $is_name = true){
    if($is_name){
        $ext = explode('.', $ext);
        $ext = $ext[1];
    }


    switch ($ext){
        case 'doc':
        case 'docx':
            return '<i class="far fa-file-word mr-2"></i>';

        case 'ppt':
        case 'pptx':
            return '<i class="far fa-file-powerpoint mr-2"></i>';

        case 'pdf':
            return '<i class="far fa-file-pdf mr-2"></i>';

        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
            return '<i class="far fa-file-image mr-2"></i>';

        case 'xls':
        case 'xlsx':
            return '<i class="far fa-file-excel mr-2"></i>';

        case 'mp4':
        case 'avi':
        case 'ogv':
        case 'webm':
        case 'flv':
            return '<i class="far fa-file-video mr-2"></i>';

        case 'mp3':
        case 'ogg':
            return '<i class="far fa-file-audio mr-2"></i>';

        case 'zip':
        case 'rar':
            return '<i class="far fa-file-archive mr-2"></i>';

        default:
            return '<i class="far fa-file-alt mr-2"></i>';

    }
}