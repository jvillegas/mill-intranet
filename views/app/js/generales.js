/**
 * Get year for copy
 */
$('#year').text((new Date()).getFullYear())

/**
 * Set datatables to spanish
 */
/**
 * DataTables
 **/

$.extend( true, $.fn.DataTable.defaults, {
    "dom": "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 text-right col-md-6'B>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
    "buttons": [
        {
            "extend": "excel",
            "className": "btn-primary"
        },
        {
            "extend": "pdf",
            "className": "btn-primary"
        },
        {
            "extend": "print",
            "text": "Imprimir",
            "className": "btn-primary"
        }
    ],
    "processing": true,
    "serverSide": true,
    "responsive": true,
    "language": {
        "lengthMenu": "Mostrar _MENU_ resultados por página",
        "zeroRecords": "No se han encontrado resultados",
        "info": "Mostrando del _START_ al _END_ de un total de _TOTAL_",
        "infoEmpty": "No hay resultados disponibles",
        "infoFiltered": "(filtrado de _MAX_ resultados)",
        "paginate": {
            "previous": "Anterior",
            "next": "Siguiente"
        },
        "search": "Buscar:",
        "processing": "Cargando..",
        "emptyTable": "No hay resultados disponibles"
    }
} );
$('body').tooltip({
    selector: '[data-toggle="tooltip"]'
});
$('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    // other options
});
$('.collapse').collapse({
    toggle: false
})
$(document).on('click','table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>td:first-child', function () {
    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        // other options
    });
})
$('.toast').toast()

function submitForm(form){
    var url = form.attr("action");
    var formData = {};
    var success = false;

    $(form).find("input[name]").each(function (index, node) {
        formData[node.name] = node.value;
    });
    var attr = form.attr('data-loader');

    console.log(attr)

    if (typeof attr !== typeof undefined && attr !== false) {
        console.log('er')
        $(attr).fadeIn()
    }
    $.ajax({
        type: 'POST',
        url: url,
        data: formData,
        success: function (data) {
            if (typeof attr !== typeof undefined && attr !== false) {
                $(attr).fadeOut()
            }
            data = JSON.parse(data)

            if(data.response === 'success'){
                success = true;
                let html = '<div class="toast toast-success" id="'+ data.id +'" role="alert" aria-live="assertive" aria-atomic="true">' +
                    '        <div class="toast-header">' +
                    '            <strong class="mr-auto"> '+ data.msg +'</strong>' +
                    '            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">' +
                    '                <span aria-hidden="true">&times;</span>' +
                    '            </button>' +
                    '        </div>' +
                    '    </div>'
                $(html).appendTo("#toast_container")
                let notification = $('#' + data.id)
                notification.toast({
                    delay: 2000
                })
                notification.toast('show')
            }else{

            }
        },
        async:false
    });
    return success
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function getUrlParam(parameter, defaultvalue){
    var urlparameter = defaultvalue;
    if(window.location.href.indexOf(parameter) > -1){
        urlparameter = getUrlVars()[parameter];
    }
    return urlparameter;
}

function checkUrlVariables(){
    let mode = getUrlParam('mode', false)

    let object = getUrlParam('object', false)

    let html = '<div class="toast toast-success" id="{{id}}" role="alert" aria-live="assertive" aria-atomic="true">' +
        '        <div class="toast-header">' +
        '            <strong class="mr-auto"> {{msg}} </strong>' +
        '            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">' +
        '                <span aria-hidden="true">&times;</span>' +
        '            </button>' +
        '        </div>' +
        '    </div>'
    if(mode && object){

        switch (mode){
            case 'borrar':
                if (object === 'venta'){

                    html = html.replace('{{msg}}', 'La venta se ha elminado correctamente').replace('{{id}}', 'borrar_venta')


                }else if(object === 'contacto'){
                    html = html.replace('{{msg}}', 'El contacto se ha elminado correctamente').replace('{{id}}', 'borrar_venta')
                }else if(object === 'visita'){
                    html = html.replace('{{msg}}', 'El registro se ha elminado correctamente').replace('{{id}}', 'borrar_venta')
                }
                $("#toast_container").prepend(html)
                let notification = $('#' + 'borrar_venta')
                notification.toast({
                    delay: 2000
                })
                notification.toast('show')
                break;
        }
    }
}
checkUrlVariables()