function goLogin(){
    var username = $('#username').val()
    var password = $('#password').val()
    $('#login_progress').fadeIn()
    $.ajax({
        type: 'POST',
        url: 'ajax.php?mode=login',
        data:{
            username: username,
            password: password
        },
        success: function (data) {
            data = JSON.parse(data)
            if(data.response === 'success'){
                $('#login_response').html(
                    '<div class="alert alert-success" role="alert">' +
                    '  Iniciando...' +
                    '</div>'
                )
                location.reload()
            }else{
                $('#login_response').html(
                    '<div class="alert alert-danger" role="alert">' +
                    '  ' + data.msg +
                    '</div>')
                $('#login_progress').fadeOut()
            }


        },
        error: function (data) {
            $('#login_response').html(
                '<div class="alert alert-danger" role="alert">' +
                '  Ha ocurrido un error' +
                '</div>')
            $('#login_progress').fadeOut()

        }
    })
}

$('#login_form').submit(function(e){
    e.preventDefault()
    goLogin()
})
