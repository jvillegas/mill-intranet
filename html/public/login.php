<?php

if(isset($_SESSION['id_user'])){

    header('Location: ?view=index');
}
include(HTML_DIR . 'overall/header.php');

?>
<div class="container-fluid">
    <div class="row" style="height: 100vh;">
        <div class="col-sm d-flex login-side justify-content-between flex-column ">
            <div class="page_nav_container prev_page w-50" data-next="home" style="max-width: 235px">
                <div class="page_nav_container_inner">
                    <a href="https://millfinancialgroup.com/" class="page_nav_about"><i class="fas fa-chevron-left"></i> Volver al inicio</a>
                </div>
            </div>
            <h3 class="d-block text-white font-weight-light mb-4 display-4 text-right"><b class="d-block">Área</b> Agentes</h3>
        </div>
        <div class="col-sm d-flex align-items-center align-content-start flex-wrap">

            <div class="col-sm-7 mt-5 mb-5" style="margin: 0 auto"><img src="views/images/logo.png" alt=""></div>

            <div class="col-md-10" style="margin: 0 auto">
                <form class="w-100 mt-5" id="login_form" method="post">
                    <div class="form-group input-group-lg">
                        <label for="username" hidden>Usuario</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Nombre de usuario">
                    </div>
                    <div class="form-group">
                        <label for="password" hidden>Contraseña</label>
                        <input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="Contraseña">
                    </div>
                    <button type="submit" id="submit" class="btn btn-primary mb-2">Iniciar sesión</button>
                    <div id="login_response"></div>
                    <div class="progress mb-3" id="login_progress" style="height: 20px; display: none">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                    </div>
                </form>
            </div>


        </div>
    </div>
</div>
<?php include(HTML_DIR . 'overall/scripts.php'); ?>
<script type="text/javascript" src="views/app/js/login.js"></script>
<?php include(HTML_DIR . 'overall/footer.php'); ?>
