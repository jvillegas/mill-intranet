<?php

if(!isset($_SESSION['id_user'])){

    header('Location: ?view=login');
}



include(HTML_DIR . 'overall/header.php'); ?>

<?php include(HTML_DIR . 'overall/topnav.php'); ?>


<div class="row">
    <?php if(isset($_SESSION['id_user']) and $_SESSION['level'] >= ADMIN_LEVEL): ?>
        <div class="col-lg-6">
            <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                <a href="?view=users" class="w-100 au-card-title" style="background-image:url('views/images/bg-title-01.jpg');">
                    <div class="bg-overlay bg-overlay--blue"></div>
                    <h3><i class="fas fa-users"></i>Usuarios</h3>
                    <span class="au-btn-plus">
                                    <i class="zmdi zmdi-arrow-right"></i>
                                </span>
                </a>
                <div class="au-task js-list-load">
                    <div class="au-task__title">
                        <p>Adminstre los usuarios que tendrán acceso al área de agentes. Verifique las ventas de los agentes</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                <a href="?view=files" class="w-100 au-card-title" style="background-image:url('views/images/bg-title-02.jpg');">
                    <div class="bg-overlay bg-overlay--blue"></div>
                    <h3><i class="fas fa-download"></i>Archivos</h3>
                    <span class="au-btn-plus">
                                    <i class="zmdi zmdi-arrow-right"></i>
                                </span>
                </a>
                <div class="au-task js-list-load">
                    <div class="au-task__title">
                        <p>Adminstre los archivos a descargar</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                <a href="?view=categories" class="w-100 au-card-title" style="background-image:url('views/images/descargas.jpg');">
                    <div class="bg-overlay bg-overlay--blue"></div>
                    <h3><i class="zmdi zmdi-group"></i>Empresas</h3>
                    <span class="au-btn-plus">
                                    <i class="zmdi zmdi-arrow-right"></i>
                                </span>
                </a>
                <div class="au-task js-list-load">
                    <div class="au-task__title">
                        <p>Las categorías de los archivos son las forma en se van a agrupar cuando se muestren en la pantalla de descargas</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                <a href="?view=sales" class="w-100 au-card-title" style="background-image:url('views/images/login-back.jpg');">
                    <div class="bg-overlay bg-overlay--blue"></div>
                    <h3><i class="zmdi zmdi-file-plus"></i>Ventas</h3>
                    <span class="au-btn-plus">
                                    <i class="zmdi zmdi-arrow-right"></i>
                                </span>
                </a>
                <div class="au-task js-list-load">
                    <div class="au-task__title">
                        <p>Registre sus propias ventas realizadas y verifique las ventas de otros usuarios</p>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <div class="col-lg-6">
            <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                <a href="?view=files" class="w-100 au-card-title" style="background-image:url('views/images/bg-title-02.jpg');">
                    <div class="bg-overlay bg-overlay--blue"></div>
                    <h3><i class="fas fa-download"></i>Descargas</h3>
                    <span class="au-btn-plus">
                                    <i class="zmdi zmdi-arrow-right"></i>
                                </span>
                </a>
                <div class="au-task js-list-load">
                    <div class="au-task__title">
                        <p>Descargue los archivos necesarios</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                <a href="?view=sales" class="w-100 au-card-title" style="background-image:url('views/images/login-back.jpg');">
                    <div class="bg-overlay bg-overlay--blue"></div>
                    <h3><i class="zmdi zmdi-file-plus"></i>Ventas</h3>
                    <span class="au-btn-plus">
                                    <i class="zmdi zmdi-arrow-right"></i>
                                </span>
                </a>
                <div class="au-task js-list-load">
                    <div class="au-task__title">
                        <p>Registre las ventas realizadas</p>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>


</div>


<?php include(HTML_DIR . 'overall/scripts.php'); ?>
<?php include(HTML_DIR . 'overall/footer.php'); ?>
