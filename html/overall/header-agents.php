<!DOCTYPE html>
<html lang="es" class=" cssanimations">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="https://millfinancialgroup.com/img/logo.png" type="image/png" sizes="16x16">
    <title>Mill Financial Group | Agentes</title>
    <!-- Bootstrap CSS-->
    <link href="views/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <link href="views/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="views/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="views/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link rel="stylesheet" type="text/css" href="views/vendor/DataTables/datatables.min.css"/>
    <!-- Main CSS-->
    <link href="views/css/theme.css" rel="stylesheet" media="all">
    <link href="views/app/css/style.css" rel="stylesheet" media="all">
    <style>
        .life a{
            color: #00496f;
        }
    </style>
</head>
<body>
<main>
    <nav class="navbar navbar-expand-lg navbar-dark blue-background-dark">
        <a class="navbar-brand" href="#">
            <img src="views/images/logo_blanco.png" width="60" height="30" class="d-inline-block align-top" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <?php if(isset($_GET['view']) and $_GET['view'] != 'index' and $_GET['view'] != 'agents'): ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="?view=agents">Ir al panel principal</a>
                    </li>
                <?php else: ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="https://millfinancialgroup.com/">Volver al inicio</a>
                    </li>
                <?php endif; ?>
                <li class="nav-item active">
                    <a class="nav-link" href="?view=users&mode=editar&id=<?php echo $_SESSION['id_user'] ?>">Perfil</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="?view=logout">Cerrar sesión</a>
                </li>

            </ul>
        </div>
    </nav>