<?php

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <base href="<?php echo APP_URL; ?>" target="_blank">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">
    <link rel="icon" href="https://millfinancialgroup.com/img/logo.png" type="image/png" sizes="16x16">
    <!-- Title Page-->
    <title><?php echo APP_TITLE; ?></title>

    <!-- Fontfaces CSS-->
    <link href="views/css/font-face.css" rel="stylesheet" media="all">
    <link href="views/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="views/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="views/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="views/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="views/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="views/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="views/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="views/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="views/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="views/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link rel="stylesheet" type="text/css" href="views/vendor/DataTables/datatables.min.css"/>

    <!-- Main CSS-->
    <link href="views/css/theme.css" rel="stylesheet" media="all">
    <link href="views/app/css/style.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
