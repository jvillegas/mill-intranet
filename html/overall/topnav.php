<!-- HEADER MOBILE-->
<header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
        <div class="container-fluid">
            <div class="header-mobile-inner">
                <a class="logo h-100" href="?view=index">
                    <img src="views/images/logo.png" class="img-fluid h-100" alt="Mill Financial group Admin" />
                </a>
                <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                </button>
            </div>
        </div>
    </div>
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">
                <li class="active">
                    <a href="?view=index"><i class="fas fa-tachometer-alt"></i>Inicio</a>
                </li>
                <?php if($_SESSION['level'] >= ADMIN_LEVEL): ?>
                    <li>
                        <a href="?view=users"><i class="fas fa-users"></i>Usuarios</a>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="?view=files"><i class="fas fa-download"></i>Archivos</a>
                </li>
                <?php if($_SESSION['level'] >= ADMIN_LEVEL): ?>
                    <li><a href="?view=categories"><i class="zmdi zmdi-group"></i>Empresas</a></li>
                <?php endif; ?>
                <li>
                    <a href="?view=sales" target="_self"><i class="zmdi zmdi-file-plus"></i>Ventas</a>
                </li>
                <li>
                    <a href="?view=contacts" target="_self"><i class="fas fa-users"></i>Lista de contactos</a>
                </li>
                <li><a href="?view=diary" target="_self"><i class="fas fa-book"></i>Diario</a></li>
                <li>
                    <a target="_self" href="?view=users&mode=editar&id=<?php echo $_SESSION['id_user'] ?>"><i class="zmdi zmdi-account"></i>Cuenta</a>
                </li>
                <li>
                    <a href="?view=logout" target="_self"><i class="zmdi zmdi-power"></i>Logout</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- END HEADER MOBILE-->

<!-- MENU SIDEBAR-->
<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="?view=index" class="h-100">
            <img src="views/images/logo.png" class="img-fluid h-100" alt="Mill Financial group Admin" />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <li class="active">
                    <a href="?view=index"><i class="fas fa-tachometer-alt"></i>Inicio</a>
                </li>
                <?php if($_SESSION['level'] >= ADMIN_LEVEL): ?>
                    <li>
                        <a href="?view=users"><i class="fas fa-users"></i>Usuarios</a>
                    </li>
                <?php endif; ?>
                <li><a href="?view=files"><i class="fas fa-files-o"></i>Archivos</a></li>

                <?php if($_SESSION['level'] >= ADMIN_LEVEL): ?>
                    <li><a href="?view=categories"><i class="zmdi zmdi-group"></i>Empresas</a></li>
                <?php endif; ?>
                <li><a href="?view=sales"><i class="zmdi zmdi-file-plus"></i>Ventas</a></li>
                <li><a href="?view=contacts" target="_self"><i class="fas fa-users"></i>Lista de contactos</a></li>
                <li><a href="?view=diary" target="_self"><i class="fas fa-book"></i>Diario</a></li>
            </ul>
        </nav>
    </div>
</aside>
<!-- END MENU SIDEBAR-->

<!-- PAGE CONTAINER-->
<div class="page-container">
    <!-- HEADER DESKTOP-->
    <header class="header-desktop d-sm-none d-md-block">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="header-wrap">
                    <form class="form-header" action="" method="POST" style="visibility: hidden;">
                        <input class="au-input au-input--xl" type="text" name="search" placeholder="Search for datas &amp; reports..." />
                        <button class="au-btn--submit" type="submit">
                            <i class="zmdi zmdi-search"></i>
                        </button>
                    </form>
                    <div class="header-button">
                        <div class="account-wrap">
                            <div class="account-item clearfix js-item-menu">

                                <div class="content">
                                    <a class="js-acc-btn" href="#"><?php echo $_SESSION['full_name'] ?></a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__body">
                                        <div class="account-dropdown__item">
                                            <a target="_self" href="?view=users&mode=editar&id=<?php echo $_SESSION['id_user'] ?>">
                                                <i class="zmdi zmdi-account"></i>Cuenta</a>
                                        </div>
                                    </div>
                                    <div class="account-dropdown__footer">
                                        <a href="?view=logout" target="_self"><i class="zmdi zmdi-power"></i>Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- HEADER DESKTOP-->
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
