<div class="row">
    <div class="col-md-12">
        <div class="copyright">
            <p> Copyright © <span id="year"></span> Mill Financial Group. Todos los derechos reservados. Elaborado por: <b>Todo Marketing</b>.</p>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- END MAIN CONTENT-->
</div>

<!-- Jquery JS-->
<script src="views/vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="views/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="views/vendor/bootstrap-4.1/bootstrap.min.js"></script>
<!-- Vendor JS       -->
<script src="views/vendor/slick/slick.min.js">
</script>
<script src="views/vendor/wow/wow.min.js"></script>
<script src="views/vendor/animsition/animsition.min.js"></script>
<script src="views/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script src="views/vendor/bootstrap-confirmation/bootstrap-confirmation.min.js"></script>
<script src="views/vendor/counter-up/jquery.waypoints.min.js"></script>
<script src="views/vendor/counter-up/jquery.counterup.min.js">
</script>
<script src="views/vendor/circle-progress/circle-progress.min.js"></script>
<script src="views/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="views/vendor/chartjs/Chart.bundle.min.js"></script>
<script src="views/vendor/select2/select2.min.js"></script>
<script type="text/javascript" src="views/vendor/DataTables/datatables.min.js"></script>

<!-- Datepicker -->
<link href="views/vendor/air-datepicker/dist/css/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="views/vendor/air-datepicker/dist/js/datepicker.min.js"></script>

<!-- Include English language -->
<script src="views/vendor/air-datepicker/dist/js/i18n/datepicker.es.js"></script>
<script src="views/app/js/generales.js">
</script>

<!-- Main JS-->
<script src="views/js/main.js"></script>