</main>
<div class="toast-container" id="toast_container"></div>
<!-- Jquery JS-->
<script src="views/vendor/fullcalendar/moment.min.js"></script>
<script src="views/vendor/jquery-3.2.1.min.js"></script>

<script src="views/vendor/animsition/animsition.min.js"></script>
<script src="views/vendor/circle-progress/circle-progress.min.js"></script>
<script src="views/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
<script src="views/vendor/select2/select2.min.js"></script>

<!-- Bootstrap JS-->
<script src="views/vendor/bootstrap-4.1/popper.min.js"></script>
<script src="views/vendor/bootstrap-4.1/bootstrap.min.js"></script>
<script src="views/vendor/bootstrap-confirmation/bootstrap-confirmation.min.js"></script>

<script type="text/javascript" src="views/vendor/DataTables/datatables.min.js"></script>

<!-- Datepicker -->
<link href="views/vendor/air-datepicker/dist/css/datepicker.min.css" rel="stylesheet" type="text/css">
<script src="views/vendor/air-datepicker/dist/js/datepicker.min.js"></script>

<!-- Include English language -->
<script src="views/vendor/air-datepicker/dist/js/i18n/datepicker.es.js"></script>

<!-- Fullcalendar -->
<link href="views/vendor/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css">

<script src="views/vendor/fullcalendar/fullcalendar.js"></script>
<script src="views/vendor/fullcalendar/locale-all.js"></script>
<script src="views/app/js/generales.js"></script>
<footer>
    <div class="copy row" style="margin: 0">
        <div class="col m6">
            Copyright © <span id="year"></span> Mill Financial Group. Todos los derechos reservados.
        </div>
        <div class="col m6">
            Elaborado por: <a href="https://todomkting.com/">Todo Marketing</a>
        </div>

    </div>
</footer>
<!-- Position it -->
</body>
</html>
