<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}

include(HTML_DIR . 'overall/header-agents.php');
?>

<!--<div class="page_nav_container prev_page" data-next="about" style="border: 2px solid rgba(255, 255, 255, 0.4);">
    <div class="page_nav_container_inner">
        <a href="?view=agents" style="min-width: 250px; padding: 0;" class="page_nav_about ml-1 mt-1"><i class="fas fa-chevron-left"></i> Panel principal</a>
    </div>
</div>-->
<div class="page-banner panel-blue">
    <h3 class="title"><strong>Diario - Añadir registro</strong></h3>
    <div class="about_image_container" style="width: 60%">
        <div class="about_image_container_inner">
            <div class="about_image_outer">
                <div class="about_image_inner">
                    <div class="about_image_background">
                        <div class="about_background about_background_3 trans about_image_active" style="background-image:url('views/images/bg-title-01.jpg');"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="second-part">
    <div class="container tools-container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <?php

                if(isset($_GET['success']) and $_GET['success'] == true and isset($_GET['mode']) and $_GET['mode'] == 'delete'){
                    success_msg(_('El registro se ha editado correctamente'));
                }

                ?>
                <form class="needs-validation" novalidate target="_self" action="<?php echo $_SERVER['PHP_SELF']; ?>?view=diary&mode=agregar" method="POST" enctype="application/x-www-form-urlencoded">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name">Nombre de la persona</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="" required maxlength="40">
                            <div class="invalid-feedback">Por favor ingrese un nombre</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="company">Empresa</label>
                            <input type="text" class="form-control" id="company" name="company" placeholder="" required maxlength="40">
                            <div class="invalid-feedback">Por favor ingrese una empresa</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="contact_date">Fecha en que se contactó a la persona</label>
                            <input type='text' class='form-control datepicker-here' autocomplete="off" id="contact_date" name="contact_date" required data-language='es' />
                            <div class="invalid-feedback">Por favor seleccione una fecha</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="visit">Fecha de visita <small>(Si ya se ha visitado a la persona)</small></label>
                            <input type='text' class='form-control datepicker-here' autocomplete="off" data-timepicker="true"  id="visit" name="visit" data-language='es' />
                            <div class="invalid-feedback">Por favor seleccione una fecha</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="next_visit">Próxima visita</label>
                            <input type='text' class='form-control datepicker-here' autocomplete="off" data-timepicker="true"  id="next_visit" name="next_visit" data-language='es' />
                            <div class="invalid-feedback">Por favor seleccione una fecha</div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="notes">Observaciones</label>
                            <textarea name="notes" id="notes" cols="30" rows="3" maxlength="250" class="form-control"></textarea>
                            <div class="invalid-feedback">Campo requerido</div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="results">Resultados</label>
                            <textarea name="results" id="results" cols="30" rows="3" maxlength="250" class="form-control"></textarea>
                            <div class="invalid-feedback">Campo requerido</div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<?php include(HTML_DIR . 'overall/footer-agents.php'); ?>

