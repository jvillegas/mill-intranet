<?php include(HTML_DIR . 'overall/header-agents.php'); ?>

<!--<div class="page_nav_container prev_page" data-next="about" style="border: 2px solid rgba(255, 255, 255, 0.4);">
    <div class="page_nav_container_inner">
        <a href="https://millfinancialgroup.com/" style="min-width: 220px; padding: 0;" class="page_nav_about ml-1 mt-1"><i class="fas fa-chevron-left"></i> Volver al Inicio</a>
    </div>
</div>-->
<div class="page-banner panel-blue">
    <h3 class="title">¡Bienvenido!<br><strong><?php echo $_SESSION['full_name'] ?></strong></h3>
    <div class="about_image_container" style="width: 60%">
        <div class="about_image_container_inner">
            <div class="about_image_outer">
                <div class="about_image_inner">
                    <div class="about_image_background">
                        <div class="about_background about_background_3 trans about_image_active" style="background-image: url('views/images/login-back.jpg')"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="second-part">
    <div class="container tools-container">
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <?php require_once 'modules/module.calendar.php' ?>
            </div>
            <div class="col-md-12 col-lg-6">
                <?php require_once 'modules/module.sales.php' ?>
            </div>
            <div class="col-md-12 col-lg-6">
                <?php require_once 'modules/module.contacts.php' ?>
            </div>
            <div class="col-md-12 col-lg-6">
                <?php require_once 'modules/module.downloads.php' ?>
            </div>
        </div>
    </div>
</div>

<?php include(HTML_DIR . 'overall/footer-agents.php'); ?>

<script>
    var limit = 5
    var offset = 0

    $(document).ready(function() {
        get_contacts(limit, offset)

        $('#calendar').fullCalendar({
            themeSystem: "bootstrap4",
            locale: 'es',
            local: false,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,listWeek'
            },
            editable: false,
            navLinks: true, // can click day/week names to navigate views
            eventLimit: true, // allow "more" link when too many events
            events: {
                url: '?view=agents&mode=diary_calendar',
                error: function() {
                    $('#script-warning').show();
                }
            },
            loading: function(bool) {
                $('#loading').toggle(bool);
            },
            eventDrop: function (event, delta) {
                alert(event.title + ' was moved ' + delta + ' days\n' +
                    '(should probably update your database)');
            },
            timeFormat: 'h:mm a',
            eventClick:  function(event, jsEvent, view) {
                if(event.type === 'visit'){
                    $('#modalBodyBirth').hide()
                    $('#modalBody, #visit_options').show()


                    $('#visit_notes').html(event.notes);
                    $('#visit_results').html(event.notes);

                    $('#name_modal').html(event.name);
                    $('#company_modal').html(event.company);

                    $('#contact_date_modal').html(event.contact_date);
                    $('#next_visit_modal').html(event.next_visit);
                    $('#visit_modal').html(event.visit);

                    $('#delete_event').attr('href','?view=diary&mode=borrar&id=' + event.id);
                    $('#edit_event').attr('href','?view=diary&mode=editar&id=' + event.id);
                }else{
                    $('#modalBodyBirth').show()
                    $('#modalBody, #visit_options').hide()
                    $('#email_modal').html(event.email);
                    $('#company_contact_modal').html(event.company);
                    $('#phone_modal').html(event.phone);
                }
                $('#modalTitle').html(event.title);
                $('#fullCalModal').modal();

            }
        });


        $('#next_contacts').click(function(){
            $('#prev_contacts').parent().removeClass("disabled")
            get_contacts(limit, parseInt($(this).attr("data-offset")))

        })

        $('#prev_contacts').click(function(){
            $('#next_contacts').parent().removeClass("disabled")
            get_contacts(limit, parseInt($(this).attr("data-offset")), 'prev')

        })
    });
    $('#add_contact_form').submit(function (e) {
        e.preventDefault()

        var success = submitForm($(this))

        if (success){
            $('#add_contact').modal('hide')
            $('#add_contact_form').trigger("reset");
            $('#prev_contacts').parent().addClass("disabled")
            $('#next_contacts').parent().removeClass("disabled")

        }
        get_contacts(limit, 0)

    })

    function get_contacts(limit, offset, direction = 'next', search = '') {


        $.ajax({
            type: 'POST',
            url: '?view=agents&mode=contact_list',
            data:{
                limit: limit,
                offset: offset,
                search: search
            },
            success: function (data) {
                data = JSON.parse(data)
                if(data.length > 0){

                    let  contact = $('#contact_list')

                    contact.html("")

                    for (let i = 0; i < data.length; i++){
                        contact.append('<li class="row">' +
                            '<div class="col-md-2 d-flex align-content-center align-items-center">' +
                            '<span class="mill-user big-icon"></span>' +
                            '</div>' +
                            '<div class="col-md-4">' +
                            '<p class="name">' + data[i]['full_name'] + '</p>' +
                            '<p class="number">'+ data[i]['phone'] +'</p>' +
                            '<p class="company">'+ data[i]['company'] +'</p>' +
                            '</div>' +
                            '<div class="col-md-5">' +
                            '<p class="email"><a href="'+ data[i]['email'] + '">'+ data[i]['email'] +'</a></p>' +
                            '<p class="company"><strong>Fecha de nacimiento:</strong><br> '+ data[i]['birth'] +'</p>' +
                            '</div>' +
                            '<div class="col-md-1 table-data-feature d-flex justify-content-center flex-column align-items-center align-content-center">' +
                            '<a href="?view=contacts&mode=editar&id='+ data[i]['id'] +'" class="item mb-2 mr-0" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"><i class="zmdi zmdi-edit"></i></a>' +
                            '<a href="?view=contacts&mode=borrar&id='+ data[i]['id'] +'" class="item" data-toggle="confirmation" data-title="¿Eliminar Contacto?" data-placement="top" title="" data-original-title="Eliminar"><i class="zmdi zmdi-delete"></i></a>' +
                            '</div>' +
                            '</li>')
                    }

                    $('[data-toggle=confirmation]').confirmation({
                        rootSelector: '[data-toggle=confirmation]',
                        // other options
                    });

                    $('#next_contacts').attr("data-limit", limit)

                    $('#prev_contacts').attr("data-limit", limit)
                }

                if(data.length < limit){
                    if(direction === 'next'){
                        $('#next_contacts').parent().addClass("disabled")
                        $('#prev_contacts').parent().removeClass("disabled")
                    }else{
                        $('#prev_contacts').parent().addClass("disabled")
                        $('#next_contacts').parent().removeClass("disabled")
                    }

                }
                else{
                    if(direction === 'next'){
                        $('#next_contacts')
                            .attr("data-offset", offset + limit)
                            .parent().removeClass("disabled")


                    }else{

                        $('#prev_contacts').parent().removeClass("disabled")
                    }

                }


            },
            error: function (data) {
                $('#login_response').html(
                    '<div class="alert alert-danger" role="alert">' +
                    '  Ha ocurrido un error' +
                    '</div>')
                $('#login_progress').fadeOut()

            }
        })
    }

    $('#search_contact').keyup(function () {
        get_contacts(limit, offset, 'next', $(this).val())
    })
</script>
