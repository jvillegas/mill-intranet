<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}

include(HTML_DIR . 'overall/header-agents.php');
?>

<!--<div class="page_nav_container prev_page" data-next="about" style="border: 2px solid rgba(255, 255, 255, 0.4);">
    <div class="page_nav_container_inner">
        <a href="?view=agents" style="min-width: 250px; padding: 0;" class="page_nav_about ml-1 mt-1"><i class="fas fa-chevron-left"></i> Panel principal</a>
    </div>
</div>-->
<div class="page-banner panel-blue">
    <h2 class="title text-center"><strong>Diario</strong></h2>
    <div class="about_image_container" style="width: 60%">
        <div class="about_image_container_inner">
            <div class="about_image_outer">
                <div class="about_image_inner">
                    <div class="about_image_background">
                        <div class="about_background about_background_3 trans about_image_active" style="background-image:url('views/images/bg-title-01.jpg');"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="second-part">
    <div class="container tools-container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="table-data__tool mb-4 d-flex justify-content-end">
                    <div class="table-data__tool- right">
                        <a href="?view=diary&mode=agregar" class="btn btn-success">
                            <i class="zmdi zmdi-plus"></i> Añadir registro</a>

                    </div>
                </div>
                <?php

                if(isset($_GET['success']) and $_GET['success'] == true){
                    if(isset($_GET['mode']) and $_GET['mode'] == 'delete'){
                        success_msg(_('El registro se ha eliminado correctamente'));
                    }else{
                        success_msg(_('El registro se ha agregado correctamente'));
                    }
                }

                ?>
                <div class="table-responsive">
                    <table id="table" class="fixe table text-dark table-hover">
                        <thead style="width: 100%">
                        <tr>
                            <th>N°</th>
                            <th>Nombre</th>
                            <th>Empresa</th>
                            <th>Fecho de visita</th>
                            <th>Próxima Visita</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>




            </div>
        </div>
    </div>

</div>



<script>
    window.onload = function(){
        $('#table').DataTable({
            "ajax":{
                url: 'ajax.php?mode=diary_list',
                type: 'POST',
                data:{
                    action: 'GET_LIST'
                }
            },
            "columnDefs": [
                {
                    "orderable": false,
                    "targets": [0, 5]
                }
            ],
            "aaSorting": [[1, 'asc']],
            "columns": [
                { "data": "n" },
                { "data": "name" },
                { "data": "company" },
                { "data": "visit" },
                { "data": "next_visit" },
                { "data": "edit" },
            ],
        });
    }

</script>
<?php include(HTML_DIR . 'overall/footer-agents.php'); ?>
