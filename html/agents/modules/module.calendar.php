<div class="module">
    <div class="module-header">
        <span class="mill-notebook"></span>
        <h4>Agenda</h4>
    </div>
    <div class="module-content">
        <div class="row mb-3">
            <div class="col-md-12 d-flex justify-content-between">
                <a href="?view=diary" class="btn btn-info">Ver tabla</a>
                <a href="?view=diary&mode=agregar" class="btn btn-success">
                    <i class="zmdi zmdi-plus"></i> Añadir registro</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="calendar"></div>
            </div>
        </div>

    </div>
</div>

<div id="fullCalModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modalTitle" class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
            </div>
            <div id="modalBody" class="modal-body">
                <ul class="list-group text-dark mb-4 list-group-flush">
                    <li class="list-group-item"><b id="">Nombre: </b><span id="name_modal"></span></li>
                    <li class="list-group-item"><b id="">Empresa: </b><span id="company_modal"></span></li>

                    <li class="list-group-item"><b id="">Fecha de contacto: </b><span id="contact_date_modal"></span></li>
                    <li class="list-group-item"><b id="">Pŕoxima visita: </b><span id="next_visit_modal"></span></li>
                    <li class="list-group-item"><b id="">Fecha de visita: </b><span id="visit_modal"></span></li>

                    <li class="list-group-item"><b id="">Observaciones: </b><span id="visit_notes"></span></li>
                    <li class="list-group-item"><b id="">Resultados: </b><span id="visit_results"></span></li>

                </ul>

            </div>
            <div id="modalBodyBirth" class="modal-body" style="display: none">
                <ul class="list-group text-dark mb-4 list-group-flush">
                    <li class="list-group-item"><b id="">Email: </b><span id="email_modal"></span></li>
                    <li class="list-group-item"><b id="">Empresa: </b><span id="company_contact_modal"></span></li>
                    <li class="list-group-item"><b id="">Teléfono: </b><span id="phone_modal"></span></li>
                </ul>
            </div>
            <div class="modal-footer">
                <div id="visit_options">
                    <a href="" id="delete_event" data-toggle="confirmation" data-title="¿Eliminar Registro?" class="btn btn-danger">Eliminar</a>
                    <a id="edit_event" class="btn btn-primary" target="_blank">Editar</a>
                </div>
                <button type="button" class="btn btn-secondary buttons-print btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
