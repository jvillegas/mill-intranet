<div class="module">
    <div class="module-header">
        <span class="mill-download"></span>
        <a href="?view=descargas" class="go"><h4>Descargas</h4></a>
    </div>
    <div class="module-content life list-group">
        <?php
        if(isset($files)): foreach($files as $file):?>
        <div class="list-group-item list-group-flush list-group-item-action">
            <button class="font-weight-normal text-left h2 w-100 mb-1"  data-toggle="collapse"
                data-target="#file_<?php echo $file['category_id']; ?>" aria-expanded="false" ><?php echo $file['name']; ?></button>
            <div class="row collapse hide" id="file_<?php echo $file['category_id']; ?>">
                <div class="col-md-12 p-1">
                    <div class="card w-100 border-right-0 border-left-0">
                        <div class="card-body p-2">
                            <ul class="list-group list-group-flush">
                                <?php foreach ($file['files'] as $cat_file) : ?>
                                    <li class="list-group-item p-1">
                                        <a target="_blank" href="<?php echo $cat_file['file']; ?>">
                                            <?php echo get_file_icon($cat_file['file']) . $cat_file['name']; ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php endforeach; endif; ?>
    </div>
</div>