<div class="module">
    <div class="module-header">
        <span class="mill-test"></span>
        <h4>Ventas</h4>
    </div>
    <div class="module-content">
        <button data-target="#add_sale" data-toggle="modal" type="button" class="btn btn-blue btn-block mb-4 text-center"><i class="fas fa-plus-circle"></i> Añadir venta</button>
        <table id="list_table" class="w-100 table text-dark table-hover">
            <thead class="w-100">
            <tr>
                <th>N°</th>
                <th>Fecha</th>
                <th>Cliente</th>
                <th>Monto</th>
                <th>Comisión</th>
                <th>Estado</th>

                <th>N° de póliza</th>
                <th>Prima comisionable</th>
                <th>Empresa</th>
                <th>Tipo de negocio</th>
                <th>Método de pago</th>
                <th>Número de transacción</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="view_sale" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Detalles de la venta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div id="loader" class="loader-block">
                    <div class="spinner-border text-primary" role="status">
                        <span class="sr-only">Cargando...</span>
                    </div>
                </div>
                <div class="row" id="view_content">
                    <div class="col-md-12">
                        <table class="table-top-campaign table" style="font-size: 16px">
                            <tbody>
                            <tr>
                                <td><b>Cliente:</b></td>
                                <td id="view_clients" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Empresa:</b></td>
                                <td id="view_company" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Fecha de pago:</b></td>
                                <td id="view_date" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Número de póliza:</b></td>
                                <td id="view_policy_number" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Tipo de negocio:</b></td>
                                <td id="view_business_type" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Método de pago:</b></td>
                                <td id="view_payment_method" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Número de transacción:</b></td>
                                <td id="view_transaction_number" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Monto:</b></td>
                                <td id="view_amount" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Prima comisionable:</b></td>
                                <td id="view_first_commissioner" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Observaciones:</b></td>
                                <td id="view_notes" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-success">Aceptar</button>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="add_sale" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content needs-validation" data-loader="#sale_loader" id="add_sale_form" novalidate target="_self" action="<?php echo $_SERVER['PHP_SELF']; ?>?view=sales&mode=agregar" method="POST" enctype="application/x-www-form-urlencoded">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Agregar Venta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="sale_loader" class="loader-block" style="opacity: .7; display: none">
                    <div class="spinner-border text-primary" role="status">
                        <span class="sr-only">Cargando...</span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="clients">Cliente</label>
                        <input type="text" class="form-control" id="clients" name="clients" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un cliente</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="company">Empresa</label>
                        <input type="text" class="form-control" id="company" name="company" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese una empresa</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="date">Fecha de pago</label>
                        <input type='text' class='form-control datepicker-here' id="date" name="date" required data-language='es' />
                        <div class="invalid-feedback">Por favor seleccione una fecha</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="policy_number">Número de póliza</label>
                        <input type="text" class="form-control" id="policy_number" name="policy_number" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un número de póliza</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="business_type">Tipo de negocio</label>
                        <input type="text" class="form-control" id="business_type" name="business_type" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un tipo de negocio</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="payment_method">Método de pago</label>
                        <input type="text" class="form-control" id="payment_method" name="payment_method" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un método de pago</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="transaction_number">Número de transacción</label>
                        <input type="text" class="form-control" id="transaction_number" name="transaction_number" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un número de transacción</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="amount">Monto</label>
                        <input type="number" class="form-control" id="amount" name="amount" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un monto</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="first_commissioner">Prima comisionable</label>
                        <input type="number" class="form-control" id="first_commissioner" name="first_commissioner" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Campo requerido</div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="notes">Observaciones</label>
                        <textarea name="notes" id="notes" cols="30" rows="3" maxlength="250" class="form-control"></textarea>
                        <div class="invalid-feedback">Campo requerido</div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success">Agregar</button>
            </div>
        </form>
    </div>
</div>

<script>
    window.onload = function () {

        var table = $('#list_table').DataTable({
            "pageLength": 5,
            "ajax":{
                url: 'ajax.php?mode=sales',
                type: 'POST',
            },
            "columnDefs": [
                {
                    "orderable": false,
                    "targets": [0 ,11]
                },
                { targets : [5],
                    render : function (data, type, row) {
                        return row.is_paid === 0 ? '<span style="white-space: nowrap" class="role admin">Sin pagar</span>' : '<span class="role member">Pagada</span>'
                    }
                }
            ],
            "aaSorting": [[1, 'desc']],
            "columns": [
                { "data": "n" },
                { "data": "date" },
                { "data": "clients" },
                { "data": "amount" },
                { "data": "commission" },
                { "data": "is_paid" },
                { "data": "policy_number" },
                { "data": "first_commissioner" },
                { "data": "company" },
                { "data": "business_type" },
                { "data": "payment_method" },
                { "data": "transaction_number" },
                { "data": "edit" },
            ],
        })
            .on('click', '.view_sale_link', function () {
                let sale_id = $(this).attr('data-sale')
                $('#loader').fadeIn()
                $.ajax({
                    type: 'POST',
                    url: "ajax.php?mode=sales&action=retrieve",
                    data:{
                        sale_id: sale_id
                    },
                    success: function(data){
                        data = JSON.parse(data)

                        if(data.response === 'success'){
                            $('#view_clients').text(data.sale.clients)
                            $('#view_company').text(data.sale.company)
                            $('#view_date').text(data.sale.date)
                            $('#view_policy_number').text(data.sale.policy_number)
                            $('#view_business_type').text(data.sale.business_type)
                            $('#view_payment_method').text(data.sale.payment_method)
                            $('#view_transaction_number').text(data.sale.transaction_number)
                            $('#view_amount').text(data.sale.amount)
                            $('#view_first_commissioner').text(data.sale.first_commissioner)
                            $('#view_notes').text(data.sale.notes)

                        }
                        $('#loader').fadeOut()
                    }
                })
            });

        $('#add_sale_form').submit(function (e) {
            e.preventDefault()

            var success = submitForm($(this))

            if (success){
                $('#add_sale').modal('hide')
                $('#add_sale_form').trigger("reset");
                table.ajax.reload();
            }

        })
    }
</script>