<div class="module">
  <div class="module-header">
    <span class="mill-user"></span>
    <h4>Lista de contactos</h4>
  </div>
  <div class="module-content">
    <button class="btn btn-blue btn-block text-center" data-target="#add_contact" data-toggle="modal" type="button" id="add_user"><i class="fas fa-plus-circle"></i> Añadir contacto</button>
    <div class="col-md-12">
        <div class="form-group row mt-4">
            <input type="text" class="form-control" placeholder="Buscar..." id="search_contact">
        </div>
      <ul class="contact-list" id="contact_list"></ul>

    </div>
    <nav aria-label="..." class="pull-right">
      <ul class="pagination">
        <li class="page-item disabled">
          <button id="prev_contacts" class="page-link" tabindex="-1">Anterior</button>
        </li>
        <li class="page-item">
          <button class="page-link" id="next_contacts" data-offset="5">Siguiente</button>
        </li>
      </ul>
    </nav>
  </div>
</div>

<div class="modal fade" id="add_contact" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <form class="modal-content needs-validation" id="add_contact_form" novalidate target="_self" action="<?php echo $_SERVER['PHP_SELF']; ?>?view=contacts&mode=agregar" method="POST" enctype="application/x-www-form-urlencoded">
      <div class="modal-header">
        <h5 class="modal-title" id="mediumModalLabel">Agregar contacto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="form-group col-md-6">
            <label for="full_name">Nombre</label>
            <input type="text" class="form-control" id="full_name" name="full_name" placeholder="" required maxlength="40">
            <div class="invalid-feedback">Por favor ingrese un nombre</div>
          </div>
          <div class="form-group col-md-6">
            <label for="company">Empresa</label>
            <input type="text" class="form-control" id="company" name="company" placeholder="" required maxlength="40">
            <div class="invalid-feedback">Por favor ingrese una empresa</div>
          </div>
          <div class="form-group col-md-6">
            <label for="phone">Teléfono</label>
            <input type='text' class='form-control' id="phone" name="phone" required data-language='es' />
            <div class="invalid-feedback">Por favor seleccione un teléfono</div>
          </div>
          <div class="form-group col-md-6">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" placeholder="" required maxlength="40">
            <div class="invalid-feedback">Por favor ingrese un email</div>
          </div>
          <div class="form-group col-md-6">
            <label for="birth">Fecha de nacimiento</label>
            <input type='text' placeholder="Seleccione una fecha" class='form-control datepicker-here' id="birth" readonly name="birth" required data-language='es' />
            <div class="invalid-feedback">Por favor seleccione una fecha</div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-success">Agregar</button>
      </div>
    </form>
  </div>
</div>
