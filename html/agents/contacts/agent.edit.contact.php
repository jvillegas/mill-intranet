<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}

if(!isset($contact_data)){
    header('Location: ?view=contacts');
}

include(HTML_DIR . 'overall/header-agents.php');
?>

<!--<div class="page_nav_container prev_page" data-next="about" style="border: 2px solid rgba(255, 255, 255, 0.4);">
    <div class="page_nav_container_inner">
        <a href="?view=agents" style="min-width: 250px; padding: 0;" class="page_nav_about ml-1 mt-1"><i class="fas fa-chevron-left"></i> Panel principal</a>
    </div>
</div>-->
<div class="page-banner panel-blue">
    <h3 class="title"><strong>Editar contacto</strong></h3>
    <div class="about_image_container" style="width: 60%">
        <div class="about_image_container_inner">
            <div class="about_image_outer">
                <div class="about_image_inner">
                    <div class="about_image_background">
                        <div class="about_background about_background_3 trans about_image_active" style="background-image:url('views/images/bg-title-02.jpg');"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="second-part">
    <div class="container tools-container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <?php

                if(isset($_GET['success']) and $_GET['success'] == true and isset($_GET['mode']) and $_GET['mode'] == 'editar'){
                    success_msg(_('El contacto se ha editado correctamente'));
                }

                ?>
                <form class=" needs-validation" novalidate target="_self" action="<?php echo $_SERVER['PHP_SELF']; ?>?view=contacts&mode=editar&id=<?php echo $_GET['id'] ?>" method="POST" enctype="application/x-www-form-urlencoded">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="full_name">Nombre</label>
                            <input type="text" class="form-control" id="full_name" name="full_name" placeholder="" required maxlength="140" value="<?php echo $contact_data['full_name']?>">
                            <div class="invalid-feedback">Por favor ingrese un nombre</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="company">Empresa</label>
                            <input type="text" class="form-control" id="company" name="company" placeholder="" required maxlength="40" value="<?php echo $contact_data['company']?>">
                            <div class="invalid-feedback">Por favor ingrese una empresa</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="phone">Teléfono</label>
                            <input type='text' class='form-control' id="phone" name="phone" required value="<?php echo $contact_data['phone']?>"/>
                            <div class="invalid-feedback">Por favor seleccione un teléfono</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="" required maxlength="40"  value="<?php echo $contact_data['email']?>">
                            <div class="invalid-feedback">Por favor ingrese un email</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="birth">Fecha de nacimiento</label>
                            <input type='text' placeholder="Seleccione una fecha" value="<?php echo change_date_format_to_front($contact_data['birth']); ?>" class='form-control datepicker-here' id="birth" readonly name="birth" required data-language='es' />
                            <div class="invalid-feedback">Por favor seleccione una fecha</div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>

                    </div>
                </form>


            </div>

        </div>
    </div>
</div>

<?php include(HTML_DIR . 'overall/footer-agents.php'); ?>
