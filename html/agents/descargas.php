<?php include(HTML_DIR . 'overall/header-agents.php'); ?>

    <!--<div class="page_nav_container prev_page" data-next="about" style="border: 2px solid rgba(255, 255, 255, 0.4);">
        <div class="page_nav_container_inner">
            <a href="?view=agents" style="min-width: 250px; padding: 0;" class="page_nav_about ml-1 mt-1"><i class="fas fa-chevron-left"></i> Panel principal</a>
        </div>
    </div>-->

    <div class="page-banner panel-green">
        <h3 class="title"><strong>Descargas</strong></h3>
        <div class="about_image_container" style="width: 60%">
            <div class="about_image_container_inner">
                <div class="about_image_outer">
                    <div class="about_image_inner">
                        <div class="about_image_background">
                            <div class="about_background about_background_3 trans about_image_active" style="background-image: url('views/images/descargas.jpg')"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="second-part life">
        <div class="mt-4 col-sm-10  offset-sm-1">
            <?php if(isset($files)): foreach($files as $file):?>
                <h2 class="font-weight-normal page-title h1 mb-1"><?php echo $file['name']; ?></h2>
                <div class="row">
                    <div class="col-md-12 p-1">
                        <div class="card w-100 border-right-0 border-left-0">
                            <div class="card-body p-2">
                                <ul class="list-group list-group-flush">
                                    <?php foreach ($file['files'] as $cat_file) : ?>
                                        <li class="list-group-item p-1">
                                            <a target="_blank" href="<?php echo $cat_file['file']; ?>">
                                                <?php echo get_file_icon($cat_file['file']) . $cat_file['name']; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; endif; ?>
        </div>
    </div>

<?php include(HTML_DIR . 'overall/footer-agents.php'); ?>