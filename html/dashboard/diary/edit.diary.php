<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}

if(!isset($diary_data)){
    header('Location: ?view=diary');
}

include(HTML_DIR . 'overall/header.php');

include(HTML_DIR . 'overall/topnav.php'); ?>


<div class="row">
    <div class="col-sm-12">
        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
            <div class="w-100 au-card-title" style="background-image:url('views/images/bg-title-01.jpg');">
                <div class="bg-overlay bg-overlay--blue"></div>
                <h3><i class="fas fa-users"></i>Editar registro</h3>
            </div>

            <div class="au-task p-4">
                <?php

                if(isset($_GET['success']) and $_GET['success'] == true and isset($_GET['mode']) and $_GET['mode'] == 'delete'){
                    success_msg(_('El registro se ha agregado correctamente'));
                }

                ?>
                <form class="needs-validation" novalidate target="_self" action="<?php echo $_SERVER['PHP_SELF']; ?>?view=diary&mode=agregar" method="POST" enctype="application/x-www-form-urlencoded">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name">Nombre de la persona</label>
                            <input value="<?php echo $diary_data['name']; ?>" type="text" class="form-control" id="name" name="name" placeholder="" required maxlength="40">
                            <div class="invalid-feedback">Por favor ingrese un nombre</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="company">Empresa</label>
                            <input value="<?php echo $diary_data['company']; ?>" type="text" class="form-control" id="company" name="company" placeholder="" required maxlength="40">
                            <div class="invalid-feedback">Por favor ingrese una empresa</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="contact_date">Fecha en que se contactó a la persona</label>
                            <input value="<?php echo change_date_format($diary_data['contact_date']); ?>" type='text' class='form-control datepicker-here' autocomplete="off" id="contact_date" name="contact_date" required data-language='es' />
                            <div class="invalid-feedback">Por favor seleccione una fecha</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="visit">Fecha de visita <small>(Si ya se ha visitado a la persona)</small></label>
                            <input value="<?php echo change_datetime_format_to_front($diary_data['visit']); ?>" type='text' class='form-control datepicker-here' autocomplete="off" data-timepicker="true"  id="visit" name="visit" data-language='es' />
                            <div class="invalid-feedback">Por favor seleccione una fecha</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="next_visit">Próxima visita</label>
                            <input value="<?php echo change_datetime_format_to_front($diary_data['next_visit']); ?>" type='text' class='form-control datepicker-here' autocomplete="off" data-timepicker="true"  id="next_visit" name="next_visit" data-language='es' />
                            <div class="invalid-feedback">Por favor seleccione una fecha</div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="notes">Observaciones</label>
                            <textarea name="notes" id="notes" cols="30" rows="3" maxlength="250" class="form-control"><?php echo $diary_data['notes']; ?></textarea>
                            <div class="invalid-feedback">Campo requerido</div>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="results">Resultados</label>
                            <textarea name="results" id="results" cols="30" rows="3" maxlength="250" class="form-control"><?php echo $diary_data['results']; ?></textarea>
                            <div class="invalid-feedback">Campo requerido</div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>

                    </div>
                </form>


            </div>

        </div>
    </div>
</div>


<?php include(HTML_DIR . 'overall/scripts.php'); ?>
<?php include(HTML_DIR . 'overall/footer.php'); ?>
