<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}

include(HTML_DIR . 'overall/header.php'); ?>

<?php include(HTML_DIR . 'overall/topnav.php'); ?>


<div class="row">
    <div class="col-sm-12">
        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
            <div class="w-100 au-card-title" style="background-image:url('views/images/bg-title-01.jpg');">
                <div class="bg-overlay bg-overlay--blue"></div>
                <h3><i class="fas fa-users"></i>Usuarios</h3>
            </div>

            <div class="au-task p-4">
                <div class="table-data__tool mb-4 d-flex justify-content-end">
                    <div class="table-data__tool- right">
                        <a href="?view=users&mode=agregar" class="au-btn au-btn-icon au-btn--green au-btn--small">
                            <i class="zmdi zmdi-plus"></i>Agregar usuario</a>

                    </div>
                </div>
                <?php

                if(isset($_GET['success']) and $_GET['success'] == true and isset($_GET['mode']) and $_GET['mode'] == 'delete'){
                    success_msg(_('El usuario se ha eliminado correctamente'));
                }

                ?>
                <div class="table-responsive">
                    <table id="user_table" class="fixe table text-dark table-hover">
                        <thead style="width: 100%">
                        <tr>
                            <th>N°</th>
                            <th>Usuario</th>
                            <th>Nombre y apellido</th>
                            <th>Correo electrónico</th>
                            <th>Perfil</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>


            </div>

        </div>
    </div>
</div>


<?php include(HTML_DIR . 'overall/scripts.php'); ?>
<script>
    $('#user_table').DataTable({
        "ajax":{
            url: 'ajax.php?mode=users',
            type: 'POST',
            data:{
                action: 'GET_LIST'
            }
        },
        "columnDefs": [
            {
                "orderable": false,
                "targets": [0 ,5]
            }
        ],
        "aaSorting": [[1, 'asc']],
        "columns": [
            { "data": "n" },
            { "data": "username" },
            { "data": "full_name" },
            { "data": "email" },
            { "data": "profile" },
            { "data": "edit" },
        ],
    });
</script>
<?php include(HTML_DIR . 'overall/footer.php'); ?>
