<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}

include(HTML_DIR . 'overall/header.php'); ?>

<?php include(HTML_DIR . 'overall/topnav.php'); ?>


<div class="row">
    <div class="col-sm-12">
        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
            <div class="w-100 au-card-title" style="background-image:url('views/images/login-back.jpg');">
                <div class="bg-overlay bg-overlay--blue"></div>
                <?php

                if(isset($user) and $user != null) : ?>
                    <h3><i class="zmdi zmdi-file-plus"></i>Ventas de <?php echo $user['first_name'] . ' ' .$user['last_name'] ?></h3>
                <?php else: ?>
                    <h3><i class="zmdi zmdi-file-plus"></i>Ventas</h3>
                <?php endif;?>

            </div>

            <div class="p-4">
                <div class="table- mb-4 d-flex justify-content-between justify-content-sm-between">
                    <?php if($_SESSION['level'] >= ADMIN_LEVEL and !isset($_GET['user'])): ?>
                        <div class=" right">
                            <a href="?view=sales&user=<?php echo $_SESSION['id_user'] ?>" class="au-btn btn au-btn-icon au-btn--green au-btn--small">
                                Mis ventas</a>
                        </div>
                    <?php elseif($_SESSION['level'] >= ADMIN_LEVEL): ?>
                        <div class="- right">
                            <a href="?view=sales" data-toggle="modal" type="button" class="au-btn btn au-btn-icon au-btn--green au-btn--small">
                                Todas las ventas</a>
                        </div>
                    <?php endif; ?>


                    <div class="table-data__tool- right">
                        <button data-target="#add_sale" data-toggle="modal" type="button" class="au-btn au-btn-icon au-btn--green au-btn--small">
                            <i class="zmdi zmdi-plus"></i>Agregar venta</button>

                    </div>
                </div>
                <div id="notes">
                    <?php

                    if(isset($_GET['success'])){
                        if($_GET['success'] == 'true'){
                            if(isset($_GET['mode']) and $_GET['mode'] == 'delete'){
                                success_msg(_('La venta se ha eliminado correctamente'));
                            }else{
                                success_msg(_('La venta se ha agregado correctamente'));
                            }

                        }else{
                            error_msg(isset($_GET['error'])  ? $_GET['error'] : 'Ha ocurridor un error');
                        }

                    }



                    ?>
                </div>
                <table id="list_table" class="w-100 table text-dark table-hover">
                    <thead class="w-100">
                    <tr>
                        <th>N°</th>
                        <th>Usuario</th>
                        <th>Fecha</th>
                        <th>Cliente</th>
                        <th>Empresa</th>
                        <th>N° de póliza</th>
                        <th>Monto</th>
                        <th>Prima comisionable</th>
                        <th>Comisión</th>
                        <th>Tipo de negocio</th>
                        <th>Método de pago</th>
                        <th>N° de transacción</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

            </div>

        </div>

    </div>
</div>
<?php include(HTML_DIR . 'overall/scripts.php'); ?>
<script>

    var url_string = window.location.href
    var url = new URL(url_string);
    //var c = url.searchParams.get("c");

    var table = $('#list_table').DataTable({
        "ajax":{
            url: 'ajax.php?mode=sales',
            type: 'POST',
            data:{
                user: url.searchParams.get("user")
            }
        },
        "columnDefs": [
            {
                "orderable": false,
                "targets": [0 ,13]
            },
            { targets : [12],
                render : function (data, type, row) {
                    return row.is_paid === 0 ? '<span style="white-space: nowrap" class="role admin">Sin pagar</span>' : '<span class="role member">Pagada</span>'
                }
            }
        ],
        "aaSorting": [[2, 'desc']],
        "columns": [
            { "data": "n" },
            { "data": "user" },
            { "data": "date" },
            { "data": "clients" },
            { "data": "company" },
            { "data": "policy_number" },
            { "data": "amount" },
            { "data": "first_commissioner" },
            { "data": "commission" },
            { "data": "business_type" },
            { "data": "payment_method" },
            { "data": "transaction_number" },
            { "data": "is_paid" },
            { "data": "edit" },
        ],
    })
        .on('click', '.pay_button', function () {
            var label = $(this).closest('tr').find('.role')
            label.removeClass().addClass('role member refund_button').attr('data-original-title', 'Cambiar sin pagar').text("Pagada")
            var id = $(this).attr('data-pay')
            $(this).removeClass().addClass('item refund_button').attr('data-original-title', 'Cambiar a sin pagar').html('<i class="far fa-minus-square"></i>')

            $('#notes').html(
                '<div class="progress mb-4 mt-4" id="login_progress">' +
                '<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>\n' +
                '</div>'
            )

            $.ajax({
                type: "POST",
                data: {
                    id: id
                },
                url: "ajax.php?mode=pay_sale",
                success: function(data){
                    data = JSON.parse(data)
                    if(data.response === 'success'){
                        $('#notes').html(
                            '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">'
                            +'<span class="badge badge-pill badge-success">Muy bien!</span>  '
                            +' Se ha actualizado la venta correctamente'
                            +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                            +'	<span aria-hidden="true">×</span>'
                            +'</button>'
                            +'</div>'
                        )
                    }else{
                        $('#notes').html(
                            '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">'
                            +'<span class="badge badge-pill badge-danger">Oh no!</span>  '
                            +' ' + data.msg
                            +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                            +'	<span aria-hidden="true">×</span>'
                            +'</button>'
                            +'</div>'
                        )
                    }

                },
                error: function (data) {
                    $('#notes').html(
                        '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">'
                        +'<span class="badge badge-pill badge-danger">Oh no!</span>  '
                        +' Ha ocurrido un error'
                        +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                        +'	<span aria-hidden="true">×</span>'
                        +'</button>'
                        +'</div>'
                    )
                }
            })
        })
        .on('click', '.refund_button', function () {
            var label = $(this).closest('tr').find('.role')
            label.removeClass().addClass('role admin').text("Sin pagar")
            var id = $(this).attr('data-pay')

            $(this).removeClass().addClass('item pay_button').attr('data-original-title', 'Cambiar a pagado').html('<i class="far fa-check-square"></i>')

            $('#notes').html(
                '<div class="progress mb-5 mt-5" id="login_progress">' +
                '<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>\n' +
                '</div>'
            )

            $.ajax({
                type: "POST",
                data: {
                    id: id
                },
                url: "ajax.php?mode=refund_sale",
                success: function(data){
                    data = JSON.parse(data)
                    if(data.response === 'success'){
                        $('#notes').html(
                            '<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">'
                            +'<span class="badge badge-pill badge-success">Muy bien!</span>  '
                            +' Se ha actualizado la venta correctamente'
                            +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                            +'	<span aria-hidden="true">×</span>'
                            +'</button>'
                            +'</div>'
                        )
                    }else{
                        $('#notes').html(
                            '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">'
                            +'<span class="badge badge-pill badge-danger">Oh no!</span>  '
                            +' ' + data.msg
                            +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                            +'	<span aria-hidden="true">×</span>'
                            +'</button>'
                            +'</div>'
                        )
                    }

                },
                error: function (data) {
                    $('#notes').html(
                        '<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">'
                        +'<span class="badge badge-pill badge-danger">Oh no!</span>  '
                        +' Ha ocurrido un error'
                        +'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                        +'	<span aria-hidden="true">×</span>'
                        +'</button>'
                        +'</div>'
                    )
                }
            })
        })
        .on('click', '.view_sale_link', function () {
            let sale_id = $(this).attr('data-sale')
            $('#loader').fadeIn()
            $.ajax({
                type: 'POST',
                url: "ajax.php?mode=sales&action=retrieve",
                data:{
                    sale_id: sale_id
                },
                success: function(data){
                    data = JSON.parse(data)

                    if(data.response === 'success'){
                        $('#view_clients').text(data.sale.clients)
                        $('#view_company').text(data.sale.company)
                        $('#view_date').text(data.sale.date)
                        $('#view_policy_number').text(data.sale.policy_number)
                        $('#view_business_type').text(data.sale.business_type)
                        $('#view_payment_method').text(data.sale.payment_method)
                        $('#view_transaction_number').text(data.sale.transaction_number)
                        $('#view_amount').text(data.sale.amount)
                        $('#view_first_commissioner').text(data.sale.first_commissioner)
                        $('#view_notes').text(data.sale.notes)

                    }
                    $('#loader').fadeOut()
                }
            })
        });

    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
<div class="modal fade" id="view_sale" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Detalles de la venta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div id="loader" class="loader-block">
                    <div class="spinner-border text-primary" role="status">
                        <span class="sr-only">Cargando...</span>
                    </div>
                </div>
                <div class="row" id="view_content">
                    <div class="col-md-12">
                        <table class="table-top-campaign table" style="font-size: 16px">
                            <tbody>
                            <tr>
                                <td><b>Cliente:</b></td>
                                <td id="view_clients" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Empresa:</b></td>
                                <td id="view_company" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Fecha de pago:</b></td>
                                <td id="view_date" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Número de póliza:</b></td>
                                <td id="view_policy_number" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Tipo de negocio:</b></td>
                                <td id="view_business_type" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Método de pago:</b></td>
                                <td id="view_payment_method" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Número de transacción:</b></td>
                                <td id="view_transaction_number" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Monto:</b></td>
                                <td id="view_amount" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Prima comisionable:</b></td>
                                <td id="view_first_commissioner" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            <tr>
                                <td><b>Observaciones:</b></td>
                                <td id="view_notes" class="text-left text-black-50" style="font-size: 16px"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-success">Aceptar</button>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="add_sale" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content needs-validation" novalidate target="_self" action="<?php echo $_SERVER['PHP_SELF']; ?>?view=sales&mode=agregar" method="POST" enctype="application/x-www-form-urlencoded">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Agregar Venta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="clients">Cliente</label>
                        <input type="text" class="form-control" id="clients" name="clients" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un cliente</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="company">Empresa</label>
                        <input type="text" class="form-control" id="company" name="company" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese una empresa</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="date">Fecha de pago</label>
                        <input type='text' class='form-control datepicker-here' id="date" name="date" required data-language='es' />
                        <div class="invalid-feedback">Por favor seleccione una fecha</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="policy_number">Número de póliza</label>
                        <input type="text" class="form-control" id="policy_number" name="policy_number" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un número de póliza</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="business_type">Tipo de negocio</label>
                        <input type="text" class="form-control" id="business_type" name="business_type" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un tipo de negocio</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="payment_method">Método de pago</label>
                        <input type="text" class="form-control" id="payment_method" name="payment_method" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un método de pago</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="transaction_number">Número de transacción</label>
                        <input type="text" class="form-control" id="transaction_number" name="transaction_number" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un número de transacción</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="amount">Monto</label>
                        <input type="number" class="form-control" id="amount" name="amount" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un monto</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="first_commissioner">Prima comisionable</label>
                        <input type="number" class="form-control" id="first_commissioner" name="first_commissioner" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Campo requerido</div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="notes">Observaciones</label>
                        <textarea name="notes" id="notes" cols="30" rows="3" maxlength="250" class="form-control"></textarea>
                        <div class="invalid-feedback">Campo requerido</div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success">Agregar</button>
            </div>
        </form>
    </div>
</div>

<?php include(HTML_DIR . 'overall/footer.php'); ?>
