<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}

include(HTML_DIR . 'overall/header.php'); ?>

<?php include(HTML_DIR . 'overall/topnav.php'); ?>


<div class="row">
    <div class="col-sm-12">
        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
            <div class="w-100 au-card-title" style="background-image:url('views/images/bg-title-02.jpg');">
                <div class="bg-overlay bg-overlay--blue"></div>
                <h3><i class="fas fa-files-o"></i>Archivos</h3>
            </div>

            <div class="p-4">
                <div class="table-data__tool mb-4 d-flex justify-content-end">
                    <div class="table-data__tool- right">
                        <?php if($_SESSION['level'] >= ADMIN_LEVEL){ ?>
                            <a href="?view=files&mode=agregar" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                <i class="zmdi zmdi-plus"></i>Agregar Archivo</a>
                        <?php } ?>
                    </div>
                </div>
                <?php

                if(isset($_GET['success']) and $_GET['success'] == true){
                    success_msg(_('El archivo se ha eliminado correctamente'));
                }

                ?>
                <table id="files_table" class="w-100 table text-dark table-hover">
                    <thead class="w-100">
                    <tr>
                        <th>N°</th>
                        <th>Nombre</th>
                        <th>Archivo</th>
                        <th>Grupo</th>
                        <th></th>
                    </tr>
                    </thead>
                   <tbody>

                   </tbody>
                </table>

            </div>

        </div>
    </div>
</div>


<?php include(HTML_DIR . 'overall/scripts.php'); ?>
<script>
    $('#files_table').DataTable({
        "ajax":{
            url: 'ajax.php?mode=files',
            type: 'POST',
            data:{
                action: 'GET_LIST'
            }
        },
        "columnDefs": [
            {
                "orderable": false,
                "targets": [0 ,4]
            }
        ],
        "aaSorting": [[1, 'asc']],
        "columns": [
            { "data": "n" },
            { "data": "name" },
            { "data": "file" },
            { "data": "category" },
            //{ "data": "company" },
            { "data": "edit" },
        ],
    });
</script>
<?php include(HTML_DIR . 'overall/footer.php'); ?>
