<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}

include(HTML_DIR . 'overall/header.php'); ?>

<?php include(HTML_DIR . 'overall/topnav.php'); ?>


<div class="row">
    <div class="col-sm-12">
        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
            <div class="w-100 au-card-title" style="background-image:url('views/images/bg-title-01.jpg');">
                <div class="bg-overlay bg-overlay--blue"></div>
                <h3><i class="fas fa-user"></i>Agregar usuario</h3>
            </div>
            <div class="p-4 row justify-content-center">
                <?php

                if(isset($_GET['success']) and $_GET['success'] == true){
                    success_msg(_('El usuario se ha agregado correctamente'));
                }

                ?>
                <div class="col-md-8">
                    <form id="add_user" class="row needs-validation" novalidate target="_self" action="<?php echo $_SERVER['PHP_SELF']; ?>?view=users&mode=agregar" method="POST" enctype="multipart/form-data">
                        <fieldset class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="first_name">Nombre</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="" required maxlength="40">
                                    <div class="invalid-feedback">Por favor ingrese un nombre</div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="last_name">Apellido</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="" required maxlength="40">
                                    <div class="invalid-feedback">Por favor ingrese un apellido</div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="birth_date">Fecha de nacimiento</label>
                                    <input type="text" class="form-control datepicker-here" autocomplete="off" data-language='es' id="birth_date" name="birth_date" placeholder="" required maxlength="40">
                                    <div class="invalid-feedback">Por favor ingrese una fecha de nacimiento</div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Correo electrónico</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="" required maxlength="40">
                                    <div class="invalid-feedback">Por favor ingrese un correo</div>
                                </div>
                                <div class="col-md-12">
                                    <div>
                                        <label for="control-label">Contrato</label>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="file" name="file" lang="es">
                                        <label class="custom-file-label" for="file" lang="es">Seleccionar Archivo</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="col-md-12">
                            <div class="row">
                                <h4 class="col-md-12">Dirección:</h4>
                                <div class="form-group col-md-6">
                                    <label for="city">Ciudad</label>
                                    <input type="text" class="form-control" id="city" name="city" placeholder="" required maxlength="40">
                                    <div class="invalid-feedback">Por favor ingrese una ciudad</div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="sector">Sector o urbanización</label>
                                    <input type="text" class="form-control" id="sector" name="sector" placeholder="" required maxlength="40">
                                    <div class="invalid-feedback">Por favor ingrese un sector o urbanización</div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="street">Calle</label>
                                    <input type="text" class="form-control" id="street" name="street" placeholder="" required maxlength="40">
                                    <div class="invalid-feedback">Por favor ingrese una calle</div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="home_number">Casa o apartamento</label>
                                    <input type="text" class="form-control" id="home_number" name="home_number" placeholder="" required maxlength="40">
                                    <div class="invalid-feedback">Por favor ingrese Número de habitación</div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="col-md-12">
                            <div class="row">
                                <h4 class="col-md-12">Empresas:</h4>
                                <?php

                                if(isset($companies)){
                                    foreach ($companies as $company){ ?>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" value="<?php echo $company['id']; ?>" name="companies[<?php echo $company['id']; ?>]" class="custom-control-input" id="company_<?php echo $company['id']; ?>">
                                                        <label class="custom-control-label" for="company_<?php echo $company['id']; ?>"><?php echo $company['name']; ?></label>
                                                    </div></div>
                                            </div>
                                            <input type="text" class="form-control" name="codes[<?php echo $company['id']; ?>]" aria-label="Text input with checkbox" placeholder="Código">
                                        </div>
                                    <?php }
                                }

                                ?>
                            </div>
                        </fieldset>
                        <div class="form-group col-md-6">
                            <label for="username">Usuario</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="" required maxlength="40">
                            <div class="invalid-feedback">Por favor ingrese un nombre de usuario</div>
                            <div class="text-danger" style="display: none" id="error_username">El nombre del usuario ya está en uso</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="profile">Perfil</label>
                            <select class="form-control" id="profile" name="profile" required>
                                <option value="">Seleccione</option>
                                <option value="1">Administrador</option>
                                <option value="2">Usuario</option>
                            </select>
                            <div class="invalid-feedback">Por favor seleccione un perfil</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password">Contraseña</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="" required maxlength="40" minlength="8">
                            <div class="invalid-feedback" id="password-feedback">Campo requerido. Debe ser entre 8-40 caracteres</div>
                            <small id="passwordHelpInline" class="text-muted">
                                Debe ser entre 8-40 caracteres.
                            </small>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password_2">Confirmar contraseña</label>
                            <input type="password" class="form-control" id="password_2" name="password_2" placeholder="" required maxlength="40" minlength="8">
                            <div class="invalid-feedback">Campo requerido. Debe ser entre 8-40 caracteres</div>
                            <div id="pass_validation" class="invalid-feedback text-danger"></div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Agregar</button>
                        </div>
                    </form>
                </div>

            </div>



        </div>
    </div>
</div>


<?php include(HTML_DIR . 'overall/scripts.php'); ?>
<script>
    var validate_username = false;
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    let validate_1 = form.checkValidity();

                    let pass_1 = $("#password").val()
                    let pass_2 = $("#password_2").val()

                    let validate_password = pass_1 === pass_2

                    if (validate_password === false){
                        $("#pass_validation").text("Las contraseñas no coinciden").show()

                        $("#password").addClass("is-invalid").removeClass("is-valid")
                        $("#password_2").addClass("is-invalid").removeClass("is-valid")
                    }else{
                        $("#pass_validation").hide()
                        $("#password").addClass("is-valid").removeClass("is-invalid")
                        $("#password_2").addClass("is-valid").removeClass("is-invalid")
                    }

                    if (validate_1 === false || validate_password === false || validate_username === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    $('#file').on('change',function(){
        //get the file name
        var fileName = document.getElementById("file").files[0].name;
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })

    $('#username').focusout(function () {
        var username = $(this).val()

        if (username.length > 0){
            $.ajax({
                type: "POST",
                url: "ajax.php?mode=users&action=ValidateUsername",
                data: {
                    username: username
                },
                success: function (data) {
                    data = JSON.parse(data)
                    if(data === false){
                        validate_username = true
                        $('#error_username').hide()
                    }else {
                        validate_username = false
                        $('#error_username').show()
                    }

                },
                error: function (data) {

                }
            })
        }


    })
</script>
<?php include(HTML_DIR . 'overall/footer.php'); ?>
