<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}

include(HTML_DIR . 'overall/header.php');

include(HTML_DIR . 'overall/topnav.php'); ?>


<div class="row">
    <div class="col-sm-12">
        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
            <div class="w-100 au-card-title" style="background-image:url('views/images/bg-title-01.jpg');">
                <div class="bg-overlay bg-overlay--blue"></div>
                <h3><i class="fas fa-users"></i>Diario</h3>
            </div>

            <div class="au-task p-4">
                <div class="table-data__tool mb-4 d-flex justify-content-end">
                    <div class="table-data__tool- right">
                        <a href="?view=diary&mode=agregar" class="au-btn au-btn-icon au-btn--green au-btn--small">
                            <i class="zmdi zmdi-plus"></i>Agregar registro</a>

                    </div>
                </div>
                <?php

                if(isset($_GET['success']) and $_GET['success'] == true){
                    if(isset($_GET['mode']) and $_GET['mode'] == 'delete'){
                        success_msg(_('El registro se ha eliminado correctamente'));
                    }else{
                        success_msg(_('El registro se ha agregado correctamente'));
                    }
                }

                ?>
                <div class="table-responsive">
                    <table id="table" class="fixe table text-dark table-hover">
                        <thead style="width: 100%">
                        <tr>
                            <th>N°</th>
                            <th>Nombre</th>
                            <th>Empresa</th>
                            <th>Fecho de visita</th>
                            <th>Próxima Visita</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>


            </div>

        </div>
    </div>
</div>


<?php include(HTML_DIR . 'overall/scripts.php'); ?>
<script>
    $('#table').DataTable({
        "ajax":{
            url: 'ajax.php?mode=diary_list',
            type: 'POST',
            data:{
                action: 'GET_LIST'
            }
        },
        "columnDefs": [
            {
                "orderable": false,
                "targets": [0, 5]
            }
        ],
        "aaSorting": [[1, 'asc']],
        "columns": [
            { "data": "n" },
            { "data": "name" },
            { "data": "company" },
            { "data": "visit" },
            { "data": "next_visit" },
            { "data": "edit" },
        ],
    });
</script>
<?php include(HTML_DIR . 'overall/footer.php'); ?>
