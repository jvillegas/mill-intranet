<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}

include(HTML_DIR . 'overall/header.php'); ?>

<?php include(HTML_DIR . 'overall/topnav.php'); ?>


<div class="row">
    <div class="col-sm-12">
        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
            <div class="w-100 au-card-title" style="background-image:url('views/images/bg-title-01.jpg');">
                <div class="bg-overlay bg-overlay--blue"></div>
                <h3><i class="fas fa-users"></i>Lista de contacto</h3>
            </div>

            <div class="au-task p-4">
                <div class="table-data__tool mb-4 d-flex justify-content-end">
                    <div class="table-data__tool- right">
                        <button data-target="#add_contact" data-toggle="modal" type="button" class="au-btn au-btn-icon au-btn--green au-btn--small">
                            <i class="zmdi zmdi-plus"></i>Agregar contacto</button>

                    </div>
                </div>
                <?php

                if(isset($_GET['success']) and $_GET['success'] == true){
                    if(isset($_GET['mode']) and $_GET['mode'] == 'delete'){
                        success_msg(_('El contacto se ha eliminado correctamente'));
                    }else{
                        success_msg(_('El contacto se ha agregado correctamente'));
                    }

                }

                ?>
                <div class="table-responsive">
                    <table id="user_table" class="fixe table text-dark table-hover">
                        <thead style="width: 100%">
                        <tr>
                            <th>N°</th>
                            <th>Nombre y apellido</th>
                            <th>Empresa</th>
                            <th>Correo electrónico</th>
                            <th>Teléfono</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>


            </div>

        </div>
    </div>
</div>


<?php include(HTML_DIR . 'overall/scripts.php'); ?>
<script>
    $('#user_table').DataTable({
        "ajax":{
            url: 'ajax.php?mode=contact_list',
            type: 'POST',
            data:{
                action: 'GET_LIST'
            }
        },
        "columnDefs": [
            {
                "orderable": false,
                "targets": [0, 5]
            }
        ],
        "aaSorting": [[1, 'asc']],
        "columns": [
            { "data": "n" },
            { "data": "full_name" },
            { "data": "company" },
            { "data": "email" },
            { "data": "phone" },
            { "data": "edit" },
        ],
    });
</script>

<div class="modal fade" id="add_contact" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form class="modal-content needs-validation" novalidate target="_self" action="<?php echo $_SERVER['PHP_SELF']; ?>?view=contacts&mode=agregar" method="POST" enctype="application/x-www-form-urlencoded">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Agregar contacto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="full_name">Nombre</label>
                        <input type="text" class="form-control" id="full_name" name="full_name" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un nombre</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="company">Empresa</label>
                        <input type="text" class="form-control" id="company" name="company" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese una empresa</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="phone">Teléfono</label>
                        <input type='text' class='form-control' id="phone" name="phone" required data-language='es' />
                        <div class="invalid-feedback">Por favor seleccione un teléfono</div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="" required maxlength="40">
                        <div class="invalid-feedback">Por favor ingrese un email</div>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success">Agregar</button>
            </div>
        </form>
    </div>
</div>
<?php include(HTML_DIR . 'overall/footer.php'); ?>
