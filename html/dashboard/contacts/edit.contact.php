<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}

if(!isset($contact_data)){
    header('Location: ?view=contacts');
}

include(HTML_DIR . 'overall/header.php');

include(HTML_DIR . 'overall/topnav.php'); ?>


<div class="row">
    <div class="col-sm-12">
        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
            <div class="w-100 au-card-title" style="background-image:url('views/images/bg-title-01.jpg');">
                <div class="bg-overlay bg-overlay--blue"></div>
                <h3><i class="fas fa-users"></i>Editar contacto</h3>
            </div>

            <div class="au-task p-4">
                <?php

                if(isset($_GET['success']) and $_GET['success'] == true and isset($_GET['mode']) and $_GET['mode'] == 'delete'){
                    success_msg(_('El contacto se ha editado correctamente'));
                }

                ?>
                <form class=" needs-validation" novalidate target="_self" action="<?php echo $_SERVER['PHP_SELF']; ?>?view=contacts&mode=editar&id=<?php echo $_GET['id'] ?>" method="POST" enctype="application/x-www-form-urlencoded">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="full_name">Nombre</label>
                            <input type="text" class="form-control" id="full_name" name="full_name" placeholder="" required maxlength="140" value="<?php echo $contact_data['full_name']?>">
                            <div class="invalid-feedback">Por favor ingrese un nombre</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="company">Empresa</label>
                            <input type="text" class="form-control" id="company" name="company" placeholder="" required maxlength="40" value="<?php echo $contact_data['company']?>">
                            <div class="invalid-feedback">Por favor ingrese una empresa</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="phone">Teléfono</label>
                            <input type='text' class='form-control' id="phone" name="phone" required value="<?php echo $contact_data['phone']?>"/>
                            <div class="invalid-feedback">Por favor seleccione un teléfono</div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="" required maxlength="40"  value="<?php echo $contact_data['email']?>">
                            <div class="invalid-feedback">Por favor ingrese un email</div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>

                    </div>
                </form>


            </div>

        </div>
    </div>
</div>


<?php include(HTML_DIR . 'overall/scripts.php'); ?>
<?php include(HTML_DIR . 'overall/footer.php'); ?>
