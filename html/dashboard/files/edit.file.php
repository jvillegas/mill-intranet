<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}
if(!isset($file_data)){
    header('Location: ?view=files');
}
include(HTML_DIR . 'overall/header.php'); ?>

<?php include(HTML_DIR . 'overall/topnav.php'); ?>


<div class="row">
    <div class="col-sm-12">
        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
            <div class="w-100 au-card-title" style="background-image:url('views/images/bg-title-02.jpg');">
                <div class="bg-overlay bg-overlay--blue"></div>
                <h3><i class="fas fa-file"></i>Editar Archivo</h3>
            </div>
            <div class="p-4 row justify-content-center">
                <?php
                if(isset($_GET['success']) and $_GET['success'] == true){
                    success_msg(_('El archivo se ha editado correctamente'));
                }

                ?>
                <div class="col-md-8">
                    <form id="add_user" class="row needs-validation" novalidate target="_self" action="<?php echo $_SERVER['PHP_SELF']; ?>?view=files&mode=editar&id=<?php echo $_GET['id'] ?>" method="POST" enctype="multipart/form-data">
                        <div class="form-group col-md-12">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="" required maxlength="40" value="<?php echo $file_data['name'] ?>">
                            <div class="invalid-feedback">Por favor ingrese un nombre</div>
                        </div>
                        <!--                        <div class="form-group col-md-12">-->
                        <!--                            <label for="company">Empresa</label>-->
                        <!--                            <select class="form-control" id="company" name="company" required>-->
                        <!--                                <option value="">Seleccione</option>-->
                        <!--                            </select>-->
                        <!--                            <div class="invalid-feedback">Por favor seleccione una empresa</div>-->
                        <!--                        </div>-->
                        <div class="form-group col-md-12">
                            <label for="category">Grupo</label>
                            <select class="form-control" id="category" name="category" required>
                                <option value="">Seleccione</option>
                                <?php if(isset($categories)): foreach ($categories as $category): ?>
                                    <option <?php if($file_data['category'] == $category['id']){ echo ' selected '; $category_name = $category['name'];} ?> value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
                                <?php endforeach; endif; ?>
                            </select>
                            <input type="hidden" id="category_name" name="category_name" value="<?php echo $category_name ?>">
                            <div class="invalid-feedback">Por favor seleccione un grupo</div>
                        </div>
                        <div class="col-md-12">
                            <label for="" class="d-block mt-2 mb-0">Archivo actual:</label>
                            <a href="<?php echo $file_data['file'] ?>" target="_blank"><?php echo $file_data['file'] ?></a>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="file" name="file" lang="es">
                                <label class="custom-file-label" for="file" lang="es">Seleccionar nuevo Archivo</label>
                            </div>
                        </div>
                        <div class="col-md-12 mt-2">
                            <button type="submit" class="btn btn-primary">Agregar</button>
                        </div>
                    </form>
                </div>

            </div>



        </div>
    </div>
</div>


<?php include(HTML_DIR . 'overall/scripts.php'); ?>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

    $('#file').on('change',function(){
        //get the file name
        var fileName = document.getElementById("file").files[0].name;
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })

    $('#category').on('change', function(){
        var category_name = $(this).children("option").filter(":selected").text()
        $('#category_name').val(category_name)
    })
</script>
<?php include(HTML_DIR . 'overall/footer.php'); ?>
