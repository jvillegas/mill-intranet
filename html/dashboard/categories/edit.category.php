<?php

if(!isset($_SESSION['id_user'])){
    header('Location: ?view=login');
}

if(!isset($category_data)){
    header('Location: ?view=categories');
}


include(HTML_DIR . 'overall/header.php'); ?>

<?php include(HTML_DIR . 'overall/topnav.php'); ?>


<div class="row">
    <div class="col-sm-12">
        <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
            <div class="w-100 au-card-title" style="background-image:url('views/images/descargas.jpg');">
                <div class="bg-overlay bg-overlay--blue"></div>
                <h3><i class="fas fa-file"></i>Agregar empresa</h3>
            </div>
            <div class="p-4 row justify-content-center">
                <?php

                if(isset($_GET['success']) and $_GET['success'] == true){
                    success_msg(_('La empresa se ha agregado correctamente'));
                }

                ?>
                <div class="col-md-8">
                    <form id="add_user" class="row needs-validation" novalidate target="_self" action="<?php echo $_SERVER['PHP_SELF']; ?>?view=categories&mode=editar&id=<?php echo $_GET['id'] ?>" method="POST" enctype="multipart/form-data">
                        <div class="form-group col-md-12">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="" required maxlength="40" value="<?php echo $category_data['name'] ?>">
                            <div class="invalid-feedback">Por favor ingrese un nombre</div>
                        </div>
                        <div class="col-md-12 mt-2">
                            <button type="submit" class="btn btn-primary">Modificar</button>
                        </div>
                    </form>
                </div>

            </div>



        </div>
    </div>
</div>


<?php include(HTML_DIR . 'overall/scripts.php'); ?>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

    $('#file').on('change',function(){
        //get the file name
        var fileName = document.getElementById("file").files[0].name;
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })

    $('#category').on('change', function(){
        var category_name = $(this).children("option").filter(":selected").text()
        $('#category_name').val(category_name)
    })
</script>
<?php include(HTML_DIR . 'overall/footer.php'); ?>
