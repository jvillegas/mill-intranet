<?php

if($_POST){
    require 'core/core.php';

    switch (isset($_GET['mode']) ? $_GET['mode'] : null){

        case 'login':
            require 'core/bin/ajax/goLogin.php';
            break;
        case 'users':
            if(isset($_GET['action'])){
                require 'core/bin/ajax/'. $_GET['action'] . '.php';
            }else {

                require 'core/bin/ajax/userList.php';
            }
            break;
        case 'files':
            require 'core/bin/ajax/fileList.php';
            break;
        case 'categories':
            require 'core/bin/ajax/categoryList.php';
            break;
        case 'sales':
            if(isset($_GET['action'])){
                require 'core/bin/ajax/'. $_GET['action'] . 'Sale.php';
            }else {
                require 'core/bin/ajax/salesList.php';
            }
            break;
        case 'pay_sale':
            require 'core/bin/ajax/paySale.php';
            break;
        case 'refund_sale':
            require 'core/bin/ajax/RefundSale.php';
            break;
        case 'contact_list':
            require 'core/bin/ajax/ContactList.php';
            break;
        case 'diary_list':

            require 'core/bin/ajax/diaryList.php';
            break;
        default:
            header('location: index.php');
            break;
    }
}