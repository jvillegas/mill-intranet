-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 09-02-2019 a las 12:11:17
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mill`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `company` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `name`, `company`) VALUES
(1, 'American Fidelity', NULL),
(2, 'Best Doctors', NULL),
(3, 'PA Group', NULL),
(4, 'Panamerican Life', NULL),
(5, 'Redbridge', NULL),
(6, 'World Medic Assist', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories_users`
--

CREATE TABLE `categories_users` (
  `user` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `code` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categories_users`
--

INSERT INTO `categories_users` (`user`, `category`, `code`) VALUES
(8, 1, 'America'),
(8, 2, 'best'),
(8, 5, 'red');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `name` varchar(140) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `companies`
--

INSERT INTO `companies` (`id`, `name`) VALUES
(1, 'American Fidelity'),
(2, 'Best Doctors'),
(3, 'PA Group'),
(4, 'Panamerican Life'),
(5, 'Redbridge'),
(6, 'World Medic Assist');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `user` int(11) DEFAULT NULL,
  `full_name` varchar(140) NOT NULL,
  `company` varchar(250) DEFAULT NULL,
  `email` varchar(140) DEFAULT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacts`
--

INSERT INTO `contacts` (`id`, `user`, `full_name`, `company`, `email`, `phone`) VALUES
(2, 8, 'Test pagina', 'Empresa test', 'email@muyreal.com', '2141697885');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diary`
--

CREATE TABLE `diary` (
  `id` int(11) NOT NULL,
  `contact_date` date NOT NULL,
  `next_visit` datetime DEFAULT NULL,
  `visit` datetime DEFAULT NULL,
  `name` varchar(140) DEFAULT NULL,
  `company` varchar(140) DEFAULT NULL,
  `notes` text,
  `results` text,
  `user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `diary`
--

INSERT INTO `diary` (`id`, `contact_date`, `next_visit`, `visit`, `name`, `company`, `notes`, `results`, `user`) VALUES
(1, '2019-01-18', '2019-01-25 12:18:00', '2019-01-17 13:27:00', '', 'Empresa test', 'sdhfgkjlñl{ñ', 'sdfghjkl', 8),
(2, '2019-01-18', '2019-01-25 12:18:00', '2019-01-17 13:27:00', 'persona', 'Empresa test', 'sdhfgkjlñl{ñ', 'sdfghjkl', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `name` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  `category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `files`
--

INSERT INTO `files` (`id`, `name`, `file`, `category`) VALUES
(3, 'Folleto', 'media/files/american-fidelity/2017-CI.pdf', 1),
(4, 'Díptico', 'media/files/american-fidelity/2017-DipticSelectTerm.pdf', 1),
(5, 'Prime', 'media/files/american-fidelity/2017-Prime.pdf', 1),
(6, 'Óptima Élite 90', 'media/files/american-fidelity/2017-OptimaElite90.pdf', 1),
(7, 'Lista de Exámenes Requeridos', 'media/files/best-doctors/64 Años o más - Lista de Exámenes Requeridos.pdf', 2),
(8, 'Global Care 2018', 'media/files/best-doctors/BD 2018 SOB Global Care.pdf', 2),
(9, 'Medical Care 2018', 'media/files/best-doctors/BD 2018 SOB Medical Care.pdf', 2),
(10, 'Medical Elite SPA', 'media/files/best-doctors/BD 2018 SOB Medical Elite SPA.pdf', 2),
(11, 'Premier Plus 2018', 'media/files/best-doctors/BD 2018 SOB Premier Plus.pdf', 2),
(12, 'Cambios de beneficios 2018', 'media/files/best-doctors/CAMBIOS DE BENEFICIOS ABRIL 2018.pdf', 2),
(13, 'Hipertensiòn Arterial', 'media/files/best-doctors/BD Salud - Hipertensio_n Arterial.pdf', 2),
(14, 'Código de Conducta', 'media/files/best-doctors/Code_of_Conduct_es best Doctors.pdf', 2),
(15, 'Global Care Hospitales', 'media/files/best-doctors/Code_of_Conduct_es best Doctors.pdf', 2),
(16, 'Instrucciones Transferencia bancaria', 'media/files/best-doctors/Instrucciones Para Transferencia Bancaria.pdf', 2),
(17, 'Red de Proveedores - Venezuela', 'media/files/best-doctors/Red de Proveedores - Venezuela.pdf', 2),
(18, 'Premier Health', 'media/files/pa-group/CONDICIONADO PREMIER HEALTH.pdf', 3),
(19, 'Guía para nuevos miembros 2018', 'media/files/pa-group/New_Member_Guide_Digital_SPA_011618.pdf', 3),
(20, 'Presentación', 'media/files/best-doctors/Sales Presentation BDI_2018 (Vzla).pdf', 2),
(21, 'Resumen de Beneficios Seguro de salud', 'media/files/pa-group/RESUMEN DE BENEFICIOS.Optima_SOB_SPA.pdf', 3),
(22, 'Resumen de Beneficios Ultra Care', 'media/files/pa-group/RESUMEN DE BENEFICIOS.UltraCare_SOB_SPA.pdf', 3),
(23, 'Resumen de Beneficios Secure', 'media/files/pa-group/RESUMEN DE BENFICIOS. Secure_SOB_SPA.pdf', 3),
(24, 'Premier Health Brochure', 'media/files/pa-group/Premier Health Brochure.pdf', 3),
(25, 'Cuestionario de enfermedades cardíacas', 'media/files/pa-group/Salud Internacional - Cuestionario de Enfermedades Cardiacas y Circulatorias.pdf', 3),
(26, 'Pan-American Centennial Términos', 'media/files/panamerican-life/PAIIC-Centennial-es TERMINO.pdf', 4),
(27, 'NexGen UL - Beneficios/Elementos', 'media/files/panamerican-life/PAIIC-NexGenUL-es.pdf', 4),
(28, 'Solicitud', 'media/files/panamerican-life/Solicitud ACTUALIZADA.pdf', 4),
(29, 'Conserve Aumente y Transfiera Patrimonio', 'media/files/panamerican-life/PAIIC-PreserveGrowAndTransferWealth-es.pdf', 4),
(30, 'Certificado Travel Assists - Español', 'media/files/redbridge/Certificado Travel Assist - Español.pdf', 5),
(31, 'Medical Plus $25,000', 'media/files/redbridge/RICL Medical Plus $25_2c000 Plan de Beneficios-2018.pdf', 5),
(32, 'Medical Plus $50,000', 'media/files/redbridge/RICL Medical Plus $50_2c000 Plan de Beneficios-2018.pdf', 5),
(33, 'Medical Plus', 'media/files/redbridge/Medical Plus.pdf', 5),
(34, 'Medical Plus Resumen de Beneficios', 'media/files/redbridge/MEDICALPLUS RESUMEN DE BENFICIOS.pdf', 5),
(35, 'Medical Plus $100,000 - Beneficios', 'media/files/redbridge/RICL Medical Plus $100_2c000 Plan de Beneficios-2018.pdf', 5),
(36, 'Tranquilidad de Cobertura - Venezuela', 'media/files/redbridge/Tranquilidad de Cobertura - Venezuela.pdf', 5),
(37, 'Plan Platinum 2018', 'media/files/redbridge/01-PLATINUM_2018.pdf', 5),
(38, 'Plan Silver 2018', 'media/files/redbridge/05-SILVER_2018.pdf', 5),
(39, 'Plan Protection Plus 2018', 'media/files/redbridge/04-PROTECTION_PLUS_2018.pdf', 5),
(40, 'Plan Gold 2018', 'media/files/redbridge/02-GOLD_2018.pdf', 5),
(41, 'Plan International Care 2018', 'media/files/n-a/06-INTL_CARE_2018.pdf', 5),
(42, 'Plan Select Inpatient 2018', 'media/files/redbridge/07-SELECT_2018.pdf', 5),
(43, 'Plan Critical Protection 2018', 'media/files/redbridge/08-CRITICAL_PROT_2018.pdf', 5),
(44, 'Forma Autorizacion de Pago Tarjeta y Debito Electronico', 'media/files/redbridge/Forma Autorizacion de Pago Tarjeta y Debito Electronico.pdf', 5),
(45, 'Forma Solicitud Cambio de Cobertura', 'media/files/redbridge/Forma Solicitud Cambio de Cobertura.pdf', 5),
(46, 'Forma Presentacion de Reclamo', 'media/files/redbridge/Forma Presentacion de Reclamo.pdf', 5),
(47, 'Opciones de Pago ', 'media/files/redbridge/Opciones de Pago - REDBRIDGE.PDF', 5),
(48, 'Red de ateción Médica Primaria - Venezuela Mayo 2018', 'media/files/redbridge/RED DE ATENCION MEDICA PRIMARIA REDBRIDGE VENEZUELA MAYO 2018.pdf', 5),
(49, 'Red Clínica Afiliadas', 'media/files/redbridge/RED DE CLINICAS AFILIADAS REDBRIDGE VENEZUELA MAYO 2018.pdf', 5),
(50, 'Solicitud de Seguro de Salud', 'media/files/redbridge/Solicitud de Salud SP MIB Rev. 02 2017.pdf', 5),
(51, 'Presentación Corporativa 2018', 'media/files/world-medic-assist/PRESENTACION CORPORATIVA OCTUBRE 2018.pdf', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `profiles`
--

INSERT INTO `profiles` (`id`, `name`, `level`) VALUES
(1, 'Administrador', 2),
(2, 'Usuario', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `clients` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `business_type` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_number` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `payment_method` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `user` int(11) NOT NULL,
  `is_paid` tinyint(4) NOT NULL DEFAULT '0',
  `first_commissioner` float NOT NULL,
  `amount` float NOT NULL,
  `policy_number` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sales`
--

INSERT INTO `sales` (`id`, `date`, `clients`, `company`, `business_type`, `transaction_number`, `payment_method`, `user`, `is_paid`, `first_commissioner`, `amount`, `policy_number`, `notes`) VALUES
(7, '2018-12-11 14:52:11', 'Cliente', 'Empresa', 'Tipo de negocio', '22222', 'Transferencia', 1, 0, 10, 200, '11111', NULL),
(8, '2019-01-09 15:50:04', 'cesar', 'pa', 'salud', '1', 'anual', 6, 0, 15, 3645, '131685432', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `profile` int(11) NOT NULL,
  `birth_date` date DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sector` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `home_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `first_name`, `last_name`, `hash`, `profile`, `birth_date`, `city`, `street`, `sector`, `home_number`, `file`) VALUES
(1, 'admin', '9f3a8da39c8d70afd9868da32d00e6db', 'cmill@millfinancialgroup.com', 'CESAR', 'MILL', '', 1, NULL, NULL, NULL, NULL, '', ''),
(6, 'rebecaumill', '9f3a8da39c8d70afd9868da32d00e6db', 'rebecaumill81@hotmail.com', 'REBECA', 'DE MILL', '', 2, NULL, NULL, NULL, NULL, '', ''),
(8, 'samuel', '47b4d0c9445131dec646a489805f0f52', 'samuelvillegas04@gmail.com', 'Samuel', 'Villegas', '', 1, '1997-01-09', 'Maracaibo', 'street', 'sector', 'home_numbers', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company` (`company`);

--
-- Indices de la tabla `categories_users`
--
ALTER TABLE `categories_users`
  ADD PRIMARY KEY (`user`,`category`),
  ADD KEY `category` (`category`);

--
-- Indices de la tabla `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indices de la tabla `diary`
--
ALTER TABLE `diary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indices de la tabla `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group` (`category`);

--
-- Indices de la tabla `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile` (`profile`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `diary`
--
ALTER TABLE `diary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`company`) REFERENCES `companies` (`id`);

--
-- Filtros para la tabla `categories_users`
--
ALTER TABLE `categories_users`
  ADD CONSTRAINT `categories_users_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categories_users_ibfk_2` FOREIGN KEY (`category`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `user` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `diary`
--
ALTER TABLE `diary`
  ADD CONSTRAINT `diary_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_ibfk_1` FOREIGN KEY (`category`) REFERENCES `categories` (`id`);

--
-- Filtros para la tabla `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`profile`) REFERENCES `profiles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
